#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sqlite3
import telegram
import logging
import xml.etree.ElementTree as ET
import time

def getMessage(path, msg):
	tree = ET.parse(path)
	root = tree.getroot()
	for attr in root.iter('message'):
		string = attr.get('string')
		if string == msg:
			return attr.text.replace("\n    ", "\n")


pathtodb = 'Telegram.Bot.Bookinator\\bin\\Debug\\Sessions\\sessions.db'
ruLocalePath = 'Telegram.Bot.Bookinator\Localizations\\RU-ru.locale'
enLocalePath = 'Telegram.Bot.Bookinator\\Localizations\\EN-en.locale'

# ruMessage = getMessage(ruLocalePath, "msg_new_version")
# enMessage = getMessage(enLocalePath, "msg_new_version")
ruMessage = 'Спасибо что воспользовались ботом. Если он вам понравился, <a href="https://telegram.me/storebot?start=bookinator_bot">оцените нас!</a> Для этого вам даже не придется покидать свой любимый Telegram. Вы всегда можете задать вопрос, и оставить свои предложения, воспользовавшись командой /feedback'
enMessage = 'Спасибо, что воспользовались ботом. Если он вам понравился, <a href="https://telegram.me/storebot?start=bookinator_bot">оцените нас!</a> Для этого вам даже не придется покидать свой любимый Telegram. Вы всегда можете задать вопрос, и оставить свои предложения, воспользовавшись командой /feedback'

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
bot = telegram.Bot(token='')

conn = sqlite3.connect(pathtodb)
conn.row_factory = lambda cursor, row: row[0]
c = conn.cursor()
ids = c.execute('SELECT TelegramID FROM Sessions').fetchall()
locales = c.execute('SELECT Locale FROM Sessions').fetchall()
votes = c.execute('SELECT Vote FROM Sessions').fetchall()
names = c.execute('SELECT Name FROM Sessions').fetchall()

for idx, name in enumerate(names):
	try:
		names[idx] = name.split(' : ')[1]
	except IndexError:
		names[idx] = "NonameFlag"
	print(names[idx])

f = open('stderr.txt', 'w')
for idx, id in enumerate(ids):
	time.sleep(1)
	if votes[idx] == 0:
		if locales[idx] == "ru-RU":
			try:
				hello = ""
				if names[idx] == "NonameFlag":
					hello = "Привет! "
				else:
					hello = "Привет," + " " + str(names[idx]) + "! "
				bot.sendMessage(parse_mode='HTML', chat_id=id, text=hello + ruMessage, disable_web_page_preview=True)
				# c.execute("""UPDATE Sessions SET Vote = ? WHERE TelegramID= ? """, (1,id))
				# conn.commit()
			except telegram.TelegramError as e:
				f.write(str(e) + '\n')
		elif locales[idx] == "en-EN":
			try:
				hello = ""
				if names[idx] == "NonameFlag":
					hello = "Hi! "
				else:
					hello = "Hi" + " " + str(names[idx]) + "! "
				bot.sendMessage(parse_mode='HTML', chat_id=id, text=hello + enMessage, disable_web_page_preview=True)
				# c.execute("""UPDATE Sessions SET Vote = ? WHERE TelegramID= ? """, (1,id))
				# conn.commit()
			except telegram.TelegramError as e:
				f.write(str(e) + '\n')