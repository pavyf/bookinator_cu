﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using File = System.IO.File;
using FB2Library;
using MoreLinq;
using System.Text;
using System.IO.Compression;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using Telegram.Bot.Types.Enums;
using System.Text.RegularExpressions;

namespace Telegram.Bot.Bookinator
{

    class Program
    {

        /**
        public static TelegramBotClient Bot = new TelegramBotClient("ключ"); // public bot
        /*/
        public static TelegramBotClient Bot = new TelegramBotClient("ключ"); // beta bot
        // */

        public static Dictionary<string, Metric> Statistics = new Dictionary<string, Metric>();
        public static List<Session> Sessions = new List<Session>();

        public static string BotUsername;

        static void Main(string[] args)
        {
            Run().Wait();
        }

        static async Task Run()
        {
            CommonConstants.databaseConnection.Open();
            

            User me = await Bot.GetMeAsync();
            BotUsername = me.Username;
            Console.WriteLine("Hello my name is {0}", me.Username);

            var offset = 1;

            string Reply = string.Empty;
            string Input = string.Empty;
            long ID = 0;
            long uid = 0;
            bool crashed = false;

            const int MaxSessions = 2000;
            const long MaxAllowedMemory = 7000000000;
            const int SessionPurgeTimerInterval = 100;
            int SessionPurgeTimer = 0;

            DateTime LastStatsReset = DateTime.Now;
            TimeSpan MaxStatsKeepTimespan = TimeSpan.FromDays(1);

            Statistics.Add("Updates", new Metric("updates", typeof(int)));
            Statistics.Add("UpdatesLast20Minutes", new Metric("updates during last 20 minutes", typeof(int)));
            Statistics.Add("AverageUpdatesLast20Minutes", new Metric("average updates during last 20 minutes", typeof(int)));
            Statistics.Add("UpdatesIncremental", new Metric("total updates", typeof(int)));
            Statistics.Add("Errors", new Metric("errors", typeof(int)));
            Statistics.Add("TelegramErrors", new Metric("telegram errors", typeof(int)));
            Statistics.Add("NewUsers", new Metric("new users", typeof(int)));
            Statistics.Add("Memory", new Metric("memory usage", typeof(int)));
            Statistics.Add("Messages", new Metric("messages", typeof(int)));

            using (var cc = new ConsoleCopy(String.Format(@"{0}/{1}", CommonConstants.FullPath, CommonConstants.logName)))
                while (true)
                {

                    try
                    {
                        var updates = await Bot.GetUpdatesAsync(offset);
                        int updatesCount = 0;

                        foreach (var update in updates)
                        {
                            if (updatesCount++ > 10) break;

                            Reply = string.Empty;
                            Input = string.Empty;
                            ID = 0;
                            InlineKeyboardMarkup inlineKeyboard = null;
                            ReplyMarkup replyKeyboard = null;
                            

                            UpdateType updType = UpdateType.UnknownUpdate;
                            try
                            {
                                updType = update.Type;
                            }
                            catch { offset = update.Id + 1; }
                            switch (updType)
                            {
                                case UpdateType.MessageUpdate:
                                    {
                                        ID = update.Message.Chat.Id;
                                        uid = update.Message.From.Id;
                                        if (!Sessions.Exists(x => x.ID == ID))
                                        {
                                            bool isNew;
                                            Sessions.Add(new Session(ID, Helper.GetChatName(update.Message.Chat), out isNew));
                                            if (isNew) Statistics["NewUsers"].Track(DateTime.Now);
                                        }
                                        Session session = Sessions.Find(x => x.ID == ID);
                                        if (session.oldVersion) SendMessage(session, session.locale.GetMessageString(CommonConstants.MessageNewVersion), "New version notification");
                                        session.oldVersion = false;
                                        session.age = update.Id;
                                        if (crashed)
                                        {
                                            Reply = session.locale.GetMessageString(CommonConstants.MessageError);
                                            crashed = false;
                                        }
                                        else
                                        {
                                            switch (update.Message.Type)
                                            {
                                                case MessageType.TextMessage:
                                                    {
                                                        Input = update.Message.Text;

                                                        // dirty hack!!

                                                        if (!Input.StartsWith("/stop"))

                                                        // dirty hack!!

                                                            // это кусок рефакторинга
                                                            // это кусок рефокторинга
                                                            //
                                                            //Helper.HandleUpdate(Input, session, BotUsername);
                                                            //
                                                            // по идее тут одна строчка должна заменить весь следующий свитч
                                                            // но разработка затянулась
                                                            // и мне лень тестить

                                                        switch (session.state)
                                                        {
                                                            case Session.ChatState.Commands:
                                                                {
                                                                    switch (update.Message.Text.Replace("@" + me.Username, String.Empty))
                                                                    {
                                                                        case CommonConstants.CommandStart:
                                                                            {
                                                                                string path = session.locale.GetMessageString(CommonConstants.PictureGreetings);
                                                                                SendMessage(session, String.Format("{0}{1}", CommonConstants.IsPictureFlag, path), "Welcome picture");
                                                                                Reply = session.locale.GetMessageString(CommonConstants.MessageGreetings);
                                                                                break;
                                                                            }
                                                                        case CommonConstants.CommandContinue:
                                                                            {
                                                                                Reply = session.GetNextLine();
                                                                                inlineKeyboard = new InlineKeyboardMarkup(Helper.GetInlineKeyboard(session.Position, session.Book.Lines.Count, session.IsReadingNote, session.locale));
                                                                                if (Reply == CommonConstants.EndOfFileFlag) Reply = session.locale.GetMessageString(CommonConstants.MessageEndOfFile);
                                                                                break;
                                                                            }
                                                                        case CommonConstants.CommandReset:
                                                                            {
                                                                                session.Position = 0;
                                                                                Reply = session.GetNextLine(ResetToBook: true);
                                                                                inlineKeyboard = new InlineKeyboardMarkup(Helper.GetInlineKeyboard(session.Position, session.Book.Lines.Count, session.IsReadingNote, session.locale));
                                                                                if (Reply == CommonConstants.EndOfFileFlag) Reply = session.locale.GetMessageString(CommonConstants.MessageEndOfFile);
                                                                                break;
                                                                            }
                                                                        case CommonConstants.CommandLanguage:
                                                                            {
                                                                                Reply = session.locale.GetMessageString(CommonConstants.MessageLanguage);
                                                                                break;
                                                                            }
                                                                        case CommonConstants.CommandHelp:
                                                                            {
                                                                                Reply = session.locale.GetMessageString(CommonConstants.MessageHelp);
                                                                                break;
                                                                            }
                                                                        case CommonConstants.CommandRussian:
                                                                            {
                                                                                session.ChangeLanguage(CommonConstants.LanguageSymbolRussian);
                                                                                Reply = session.locale.GetMessageString(CommonConstants.MessageGreetings);
                                                                                session.Update();
                                                                                break;
                                                                            }
                                                                        case CommonConstants.CommandEnglish:
                                                                            {
                                                                                session.ChangeLanguage(CommonConstants.LanguageSymbolEnglish);
                                                                                Reply = session.locale.GetMessageString(CommonConstants.MessageGreetings);
                                                                                session.Update();
                                                                                break;
                                                                            }
                                                                        case CommonConstants.CommandFeedback:
                                                                            {
                                                                                session.state = Session.ChatState.Feedback;
                                                                                Reply = session.locale.GetMessageString(CommonConstants.MessageFeedback);
                                                                                break;
                                                                            }
                                                                        case CommonConstants.CommandEditBook:
                                                                            {
                                                                                session.state = Session.ChatState.BookEdit;
                                                                                Reply = session.locale.GetMessageString(CommonConstants.MessageBookEdit);
                                                                                break;
                                                                            }
                                                                        case CommonConstants.CommandLibrary:
                                                                            {
                                                                                session.state = Session.ChatState.AccessLibrary;
                                                                                Reply = session.locale.GetMessageString(CommonConstants.MessageLibrary);
                                                                                replyKeyboard = Helper.GetReplyKeyboard(session.GetLibraryContent());
                                                                                break;
                                                                            }
                                                                        case CommonConstants.CommandAutoscroll:
                                                                            {
                                                                                session.StartAutoscroll();
                                                                                Reply = session.GetNextLine(ResetToBook: true);
                                                                                replyKeyboard = Helper.GetAutoscrollKeyboard();
                                                                                if (Reply == CommonConstants.EndOfFileFlag) Reply = session.locale.GetMessageString(CommonConstants.MessageEndOfFile);
                                                                                break;
                                                                            }
                                                                        case CommonConstants.CommandFasterAutoscroll:
                                                                            {
                                                                                session.AutoscrollFaster();
                                                                                break;
                                                                            }
                                                                        case CommonConstants.CommandSlowerAutoscroll:
                                                                            {
                                                                                session.AutoscrollSlower();
                                                                                break;
                                                                            }
                                                                        case CommonConstants.CommandStopAutoscroll:
                                                                            {
                                                                                session.StopAutoscroll();
                                                                                await SendMessage(session, "=====", "Break reply keyboard after autoscroll");
                                                                                Reply = session.GetNextLine(ResetToBook: true);
                                                                                inlineKeyboard = new InlineKeyboardMarkup(Helper.GetInlineKeyboard(session.Position, session.Book.Lines.Count, session.IsReadingNote, session.locale));
                                                                                if (Reply == CommonConstants.EndOfFileFlag) Reply = session.locale.GetMessageString(CommonConstants.MessageEndOfFile);
                                                                                break;
                                                                            }
                                                                        case CommonConstants.CommandDebug:
                                                                            {
                                                                                if (CommonConstants.AdminIDs.Contains(session.ID))
                                                                                {
                                                                                    Statistics["AverageUpdatesLast20Minutes"].GetGraph(DateTime.Now.AddDays(-1), DateTime.Now).Save(CommonConstants.FullPath + @"\Updates_debug.png", System.Drawing.Imaging.ImageFormat.Png);
                                                                                    Reply = String.Format("{0}{1}", CommonConstants.IsPictureFlag, @"\Updates_debug.png");
                                                                                    //session.ToggleAutoscroll();
                                                                                }
                                                                                else
                                                                                    goto default;
                                                                                break;
                                                                            }
                                                                        case CommonConstants.CommandUpdates:
                                                                            {
                                                                                if (CommonConstants.AdminIDs.Contains(session.ID))
                                                                                {
                                                                                    //Statistics["Updates"].GetAverageTotalByIntervalGraph(DateTime.Now.AddDays(-1), DateTime.Now, TimeSpan.FromMinutes(20)).Save(CommonConstants.FullPath + @"\Updates.png", System.Drawing.Imaging.ImageFormat.Png);
                                                                                    Statistics["UpdatesLast20Minutes"].GetGraph(DateTime.Now.AddDays(-1), DateTime.Now).Save(CommonConstants.FullPath + @"\Updates.png", System.Drawing.Imaging.ImageFormat.Png);
                                                                                    Reply = String.Format("{0}{1}", CommonConstants.IsPictureFlag, @"\Updates.png");
                                                                                }
                                                                                else
                                                                                    goto default;
                                                                                break;
                                                                            }
                                                                        case CommonConstants.CommandMemory:
                                                                            {
                                                                                if (CommonConstants.AdminIDs.Contains(session.ID))
                                                                                {
                                                                                    Statistics["Memory"].GetGraph(DateTime.Now.AddDays(-1), DateTime.Now).Save(CommonConstants.FullPath + @"\Memory.png", System.Drawing.Imaging.ImageFormat.Png);
                                                                                    //Statistics["Memory"].GetGraph(DateTime.Now.AddHours(-1), DateTime.Now).Save(CommonConstants.FullPath + @"\Memory.png", System.Drawing.Imaging.ImageFormat.Png);
                                                                                    Reply = String.Format("{0}{1}", CommonConstants.IsPictureFlag, @"\Memory.png");
                                                                                }
                                                                                else
                                                                                    goto default;
                                                                                break;
                                                                            }
                                                                        case CommonConstants.CommandStatus:
                                                                            {
                                                                                if (CommonConstants.AdminIDs.Contains(session.ID))
                                                                                {
                                                                                    StringBuilder StatusReport = new StringBuilder();
                                                                                    StatusReport.AppendFormat("Active sessions: <b>{1}</b>{0}", Environment.NewLine, Sessions.Count);
                                                                                    StatusReport.AppendFormat("Active sessions with custom books: <b>{1}</b>{0}", Environment.NewLine, Sessions.FindAll(x => (x.Book != x.locale.defaultBook)).Count);
                                                                                    StatusReport.AppendFormat("Allocated memory: <b>{1} KB</b>{0}", Environment.NewLine, Math.Round(GC.GetTotalMemory(false)/1024.0, 2));
                                                                                    StatusReport.AppendFormat("Updates during last hour: <b>{1}</b>{0}", Environment.NewLine, Statistics["Updates"].GetTotal(DateTime.Now.AddHours(-1), DateTime.Now));
                                                                                    StatusReport.AppendFormat("Sent messages during last hour: <b>{1}</b>{0}", Environment.NewLine, Statistics["Messages"].GetTotal(DateTime.Now.AddHours(-1), DateTime.Now));
                                                                                    StatusReport.AppendFormat("Out of which errors: <b>{1}</b>{0}", Environment.NewLine, Statistics["Errors"].GetTotal(DateTime.Now.AddHours(-1), DateTime.Now));
                                                                                    StatusReport.AppendFormat("Out of which telegram-related errors: <b>{1}</b>{0}", Environment.NewLine, Statistics["TelegramErrors"].GetTotal(DateTime.Now.AddHours(-1), DateTime.Now));
                                                                                    StatusReport.AppendFormat("New users today: <b>{1}</b>{0}", Environment.NewLine, Statistics["NewUsers"].GetTotal(DateTime.Now.AddDays(-1), DateTime.Now));
                                                                                    StatusReport.AppendFormat("Other info; use with caution, might cause freezes{0}", Environment.NewLine);
                                                                                    StatusReport.AppendFormat("Graph of memory usage, 24 hour span: {1}{0}", Environment.NewLine, CommonConstants.CommandMemory);
                                                                                    StatusReport.AppendFormat("Graph of average amount of updates in 20 minutes, 24 hour span: {1}{0}", Environment.NewLine, CommonConstants.CommandUpdates);
                                                                                    StatusReport.AppendFormat("WiP attempt to normalize updates graph: {1}{0}", Environment.NewLine, CommonConstants.CommandDebug);
                                                                                    //StatusReport.AppendFormat("Active sessions: <b>{1}</b>{0}", Environment.NewLine, Sessions.Count);
                                                                                    Reply = StatusReport.ToString().Trim();
                                                                                }
                                                                                else
                                                                                    goto default;
                                                                                break;                                                                                  
                                                                            }
                                                                        default:
                                                                            {
                                                                                int page = 0;
                                                                                if (update.Message.Text.StartsWith("/") && Int32.TryParse(update.Message.Text.Remove(0, 1), out page))
                                                                                {
                                                                                    if ((page > 0) && (page <= session.Book.Lines.Count))
                                                                                    {
                                                                                        Bot.EditMessageReplyMarkupAsync(ID, update.Message.MessageId);
                                                                                        session.Position = page - 1;
                                                                                        Reply = session.GetNextLine(ResetToBook: true);
                                                                                        inlineKeyboard = new InlineKeyboardMarkup(Helper.GetInlineKeyboard(session.Position, session.Book.Lines.Count, session.IsReadingNote, session.locale));
                                                                                        if (Reply == CommonConstants.EndOfFileFlag) Reply = session.locale.GetMessageString(CommonConstants.MessageEndOfFile);
                                                                                    }
                                                                                    else
                                                                                        Reply = session.locale.GetMessageString(CommonConstants.MessagePageNotFound);
                                                                                }
                                                                                else
                                                                                {
                                                                                    if (update.Message.Text.StartsWith("/") && session.Book.IsNote(update.Message.Text.Replace("/", "")))
                                                                                    {
                                                                                        Bot.EditMessageReplyMarkupAsync(ID, update.Message.MessageId);
                                                                                        Reply = session.GetNextLine(noteID: update.Message.Text.Replace("/", ""));
                                                                                        inlineKeyboard = new InlineKeyboardMarkup(Helper.GetInlineKeyboard(session.Position, session.Book.Lines.Count, session.IsReadingNote, session.locale));
                                                                                        if (Reply == CommonConstants.EndOfFileFlag) Reply = session.locale.GetMessageString(CommonConstants.MessageEndOfFile);
                                                                                    }
                                                                                    else 
                                                                                        Reply = session.locale.GetMessageString(CommonConstants.MessageDefault);
                                                                                }
                                                                                break;
                                                                            }
                                                                    }
                                                                    break;
                                                                }
                                                            case Session.ChatState.Feedback:
                                                                {
                                                                    if (update.Message.Text.Replace("@" + me.Username, String.Empty) == CommonConstants.CommandCancel)
                                                                    {
                                                                        session.state = Session.ChatState.Commands;
                                                                    }
                                                                    else
                                                                    {
                                                                        File.AppendAllText(String.Format(@"{0}/{1}", CommonConstants.FullPath, CommonConstants.FeedbackFile),
                                                                                            "\r\n\r\n================================\r\n"+
                                                                                            String.Format("{0} {1}, UID = {2}\r\n", update.Message.From.FirstName,
                                                                                            update.Message.From.LastName, uid) +
                                                                                            update.Message.Text);
                                                                        foreach (int ChatID in CommonConstants.TeamIDs)
                                                                        {
                                                                            Bot.ForwardMessageAsync(ChatID, update.Message.Chat.Id, update.Message.MessageId);
                                                                        }
                                                                        session.state = Session.ChatState.Commands;
                                                                        Reply = session.locale.GetMessageString(CommonConstants.MessageFeedbackAccepted);
                                                                        inlineKeyboard = new InlineKeyboardMarkup(Helper.GetInlineKeyboard(session.Position, session.Book.Lines.Count, session.IsReadingNote, session.locale));
                                                                    }
                                                                    break;
                                                                }
                                                            case Session.ChatState.BookEdit:
                                                                {
                                                                    if (update.Message.Text.Replace("@" + me.Username, String.Empty) == CommonConstants.CommandCancel)
                                                                    {
                                                                        session.state = Session.ChatState.Commands;
                                                                        Reply = session.locale.GetMessageString(CommonConstants.MessageActionCancelled);
                                                                        inlineKeyboard = new InlineKeyboardMarkup(Helper.GetInlineKeyboard(session.Position, session.Book.Lines.Count, session.IsReadingNote, session.locale));
                                                                    }
                                                                    else
                                                                    {
                                                                        session.RemoveBookFromLibrary(session.Book.id);
                                                                        session.Book.setName(update.Message.Text);
                                                                        session.SaveCurrentBookToLibrary();
                                                                        session.state = Session.ChatState.Commands;
                                                                        Reply = session.locale.GetMessageString(CommonConstants.MessageBookNameConfirmed);
                                                                        inlineKeyboard = new InlineKeyboardMarkup(Helper.GetInlineKeyboard(session.Position, session.Book.Lines.Count, session.IsReadingNote, session.locale));
                                                                    }
                                                                    break;
                                                                }
                                                            case Session.ChatState.AccessLibrary:
                                                                {
                                                                    if (update.Message.Text.Replace("@" + me.Username, String.Empty) == CommonConstants.CommandCancel)
                                                                    {
                                                                        session.state = Session.ChatState.Commands;
                                                                        Reply = session.locale.GetMessageString(CommonConstants.MessageActionCancelled);
                                                                        //inlineKeyboard = new InlineKeyboardMarkup(GetInlineKeyboard(session.Position, session.Book.Lines.Count, session.IsReadingNote));
                                                                    }
                                                                    else
                                                                    {
                                                                        int BookIndex = Helper.parseLibraryLineToIndex(update.Message.Text, session.GetLibraryContent());
                                                                        if (BookIndex != -1)
                                                                        {
                                                                            await SendMessage(session, session.LibraryInfo[BookIndex].Name, "Book's name, designated to clear a reply keyboard");
                                                                            Reply = String.Format(session.locale.GetMessageString(CommonConstants.MessageBookOptions), session.LibraryInfo[BookIndex].Position);
                                                                            inlineKeyboard = new InlineKeyboardMarkup(Helper.GetBookOptionsKeyboard(session.LibraryInfo[BookIndex].ID, session.locale));
                                                                        }
                                                                        else
                                                                            Reply = session.locale.GetMessageString(CommonConstants.MessageDefault);
                                                                    }
                                                                    break;
                                                                }
                                                        }
                                                        
                                                        break;
                                                    }
                                                case MessageType.DocumentMessage:
                                                    {
                                                        Input = update.Message.Document.FileName;
                                                        session.state = Session.ChatState.Commands;
                                                        session.GetNextLine(ResetToBook: true);
                                                        Types.File f = await Bot.GetFileAsync(update.Message.Document.FileId);
                                                        Stream filestream = f.FileStream;
                                                        if (update.Message.Document.FileName.EndsWith(".txt"))
                                                        {
                                                            session.NewBook(filestream, CommonConstants.Filetype.TXT);
                                                        }
                                                        else if (update.Message.Document.FileName.EndsWith(".fb2"))
                                                        {
                                                            session.NewBook(filestream, CommonConstants.Filetype.FB2);
                                                        }
                                                        else if (update.Message.Document.FileName.EndsWith(".epub"))
                                                        {
                                                            session.NewBook(filestream, CommonConstants.Filetype.EPUB);
                                                        }
                                                        else if (update.Message.Document.FileName.EndsWith(".mobi"))
                                                        {
                                                            session.NewBook(filestream, CommonConstants.Filetype.MOBI);
                                                        }
                                                        else if ((update.Message.Document.FileName.EndsWith(".fb2.zip")) && (update.Message.ForwardFrom != null && update.Message.ForwardFrom.Username == "flibustafreebookbot"))
                                                        {
                                                            using (ZipArchive archive = new ZipArchive(filestream))
                                                            {
                                                                foreach (ZipArchiveEntry entry in archive.Entries)
                                                                {
                                                                    if (entry.Name.EndsWith(".fb2"))
                                                                    {
                                                                        var stream = entry.Open();
                                                                        session.NewBook(stream, CommonConstants.Filetype.FB2);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            Reply = session.locale.GetMessageString(CommonConstants.MessageFormatNotSupported);
                                                            break;
                                                        }

                                                        if ((session.Book != null) && (!session.Book.broken))
                                                        {
                                                            Reply = session.locale.GetMessageString(CommonConstants.MessageNewBook).Replace(CommonConstants.LocaleFlagBookName, session.Book.getName());
                                                            inlineKeyboard = new InlineKeyboardMarkup(Helper.GetInlineKeyboard(session.Position, session.Book.Lines.Count, session.IsReadingNote, session.locale));
                                                            session.SaveCurrentBookToLibrary();
                                                            if (!String.IsNullOrWhiteSpace(session.Book.cover)) SendMessage(session, String.Format("{0}{1}", CommonConstants.IsPictureFlag, session.Book.cover), "Book cover");
                                                        }
                                                        else
                                                        {
                                                            Reply = session.locale.GetMessageString(CommonConstants.MessageError);
                                                        }
                                                        break;
                                                    }
                                            }
                                        }
                                        break;
                                    }

                                case UpdateType.CallbackQueryUpdate:
                                    {
                                        ID = update.CallbackQuery.Message.Chat.Id;
                                        uid = update.CallbackQuery.From.Id;
                                        if (!Sessions.Exists(x => x.ID == ID))
                                        {
                                            bool isNew;
                                            Sessions.Add(new Session(ID, Helper.GetChatName(update.CallbackQuery.Message.Chat), out isNew));
                                            if (isNew) Statistics["NewUsers"].Track(DateTime.Now);
                                        }
                                        Session session = Sessions.Find(x => x.ID == ID);
                                        if (session.oldVersion) SendMessage(session, session.locale.GetMessageString(CommonConstants.MessageNewVersion), "New version notification");
                                        session.oldVersion = false;
                                        session.age = update.Id;
                                        if ((session.state == Session.ChatState.BookEdit)||(session.state == Session.ChatState.Feedback)) session.state = Session.ChatState.Commands;
                                        Input = update.CallbackQuery.Data;
                                        //Bot.EditMessageReplyMarkup(ID, update.CallbackQuery.Message.MessageId, replyMarkup: null);
                                        if (crashed)
                                        {
                                            Reply = session.locale.GetMessageString(CommonConstants.MessageError);
                                            crashed = false;
                                        }
                                        else
                                        {
                                            switch (update.CallbackQuery.Data)
                                            {
                                                case CommonConstants.CallbackQueryContinue:
                                                    {
                                                        Reply = session.GetNextLine();
                                                        inlineKeyboard = new InlineKeyboardMarkup(Helper.GetInlineKeyboard(session.Position, session.Book.Lines.Count, session.IsReadingNote, session.locale));
                                                        if (Reply == CommonConstants.EndOfFileFlag) Reply = session.locale.GetMessageString(CommonConstants.MessageEndOfFile);
                                                        break;
                                                    }
                                                case CommonConstants.CallbackQuerySkipNote:
                                                    {
                                                        Reply = session.GetNextLine(ResetToBook: true);
                                                        inlineKeyboard = new InlineKeyboardMarkup(Helper.GetInlineKeyboard(session.Position, session.Book.Lines.Count, session.IsReadingNote, session.locale));
                                                        if (Reply == CommonConstants.EndOfFileFlag) Reply = session.locale.GetMessageString(CommonConstants.MessageEndOfFile);
                                                        break;
                                                    }
                                                case CommonConstants.CallbackQueryReset:
                                                    {
                                                        session.Position = 0;
                                                        Reply = session.GetNextLine(ResetToBook: true);
                                                        inlineKeyboard = new InlineKeyboardMarkup(Helper.GetInlineKeyboard(session.Position, session.Book.Lines.Count, session.IsReadingNote, session.locale));
                                                        if (Reply == CommonConstants.EndOfFileFlag) Reply = session.locale.GetMessageString(CommonConstants.MessageEndOfFile);
                                                        break;
                                                    }
                                                case CommonConstants.CallbackQueryMenu:
                                                    {
                                                        if (session.Book.Contents.Count == 0)
                                                        {
                                                            Reply = session.locale.GetMessageString(CommonConstants.MessageMenu);
                                                        }
                                                        else
                                                        {
                                                            foreach (string line in session.Book.Contents)
                                                                Reply += (line + "\r\n");
                                                        }
                                                        break;
                                                    }
                                                case CommonConstants.CallbackQueryToggleAutoscroll:
                                                    {
                                                        session.ToggleAutoscroll();
                                                        Reply = session.GetNextLine(ResetToBook: true);
                                                        inlineKeyboard = new InlineKeyboardMarkup(Helper.GetInlineKeyboard(session.Position, session.Book.Lines.Count, session.IsReadingNote, session.locale));
                                                        if (Reply == CommonConstants.EndOfFileFlag) Reply = session.locale.GetMessageString(CommonConstants.MessageEndOfFile);
                                                        break;
                                                    }
                                                case CommonConstants.CallbackQueryFaster:
                                                    {
                                                        session.AutoscrollFaster();
                                                        break;
                                                    }
                                                case CommonConstants.CallbackQuerySlower:
                                                    {
                                                        session.AutoscrollSlower();
                                                        break;
                                                    }
                                                case CommonConstants.CallbackQueryCancel:
                                                    {
                                                        session.state = Session.ChatState.Commands;
                                                        Reply = session.locale.GetMessageString(CommonConstants.MessageActionCancelled);
                                                        inlineKeyboard = new InlineKeyboardMarkup(Helper.GetInlineKeyboard(session.Position, session.Book.Lines.Count, session.IsReadingNote, session.locale));
                                                        break;
                                                    }
                                                default:
                                                    {
                                                        if (update.CallbackQuery.Data.StartsWith(CommonConstants.CallbackQueryReadBook))
                                                        {
                                                            session.SwitchToBookFromLibrary(update.CallbackQuery.Data.Replace(CommonConstants.CallbackQueryReadBook, ""));
                                                            session.state = Session.ChatState.Commands;
                                                            Reply = session.GetNextLine(ResetToBook: true);
                                                            inlineKeyboard = new InlineKeyboardMarkup(Helper.GetInlineKeyboard(session.Position, session.Book.Lines.Count, session.IsReadingNote, session.locale));
                                                            if (Reply == CommonConstants.EndOfFileFlag) Reply = session.locale.GetMessageString(CommonConstants.MessageEndOfFile);
                                                        }
                                                        else if (update.CallbackQuery.Data.StartsWith(CommonConstants.CallbackQueryDeleteBook))
                                                        {
                                                            Reply = session.locale.GetMessageString(CommonConstants.MessageConfirmDelete);
                                                            inlineKeyboard = new InlineKeyboardMarkup(Helper.GetConfirmKeyboard(CommonConstants.DeleteBookFlag + update.CallbackQuery.Data.Replace(CommonConstants.CallbackQueryDeleteBook, ""), session.locale));
                                                        }
                                                        else if (update.CallbackQuery.Data.StartsWith(CommonConstants.CallbackQueryConfirm))
                                                        {
                                                            string ConfirmData = update.CallbackQuery.Data.Replace(CommonConstants.CallbackQueryConfirm, "");
                                                            if (ConfirmData.StartsWith(CommonConstants.DeleteBookFlag))
                                                            {
                                                                ConfirmData = ConfirmData.Replace(CommonConstants.DeleteBookFlag, "");
                                                                session.RemoveBookFromLibrary(ConfirmData);
                                                                session.state = Session.ChatState.Commands;
                                                                Reply = session.locale.GetMessageString(CommonConstants.MessageBookDeleted);
                                                                inlineKeyboard = new InlineKeyboardMarkup(Helper.GetInlineKeyboard(session.Position, session.Book.Lines.Count, session.IsReadingNote, session.locale));
                                                            }
                                                        }
                                                        break;
                                                    }
                                            }
                                        }
                                        break;
                                    }
                            }


                            //if (!String.IsNullOrWhiteSpace(Reply))
                            //{
                            //    if (Reply.Contains(CommonConstants.IsPictureFlag))
                            //    {
                            //        string path = CommonConstants.FullPath + Reply.Replace(CommonConstants.IsPictureFlag, "");
                            //        await Bot.SendPhoto(ID, new FileToSend(path, File.Open(path, FileMode.Open)), replyMarkup: inlineKeyboard);
                            //    }
                            //    else
                            //    {
                            //        try
                            //        {
                            //            if (inlineKeyboard == null)
                            //            {
                            //                if (replyKeyboard == null)
                            //                    await Bot.SendTextMessage(ID, Reply, replyMarkup: new ReplyKeyboardHide(),parseMode: ParseMode.Html);
                            //                else
                            //                    await Bot.SendTextMessage(ID, Reply, replyMarkup: replyKeyboard, parseMode: ParseMode.Html);
                            //            }
                            //            else
                            //            {
                            //                Message msg = await Bot.SendTextMessage(ID, Reply, replyMarkup: inlineKeyboard, parseMode: ParseMode.Html);
                            //            }
                            //        }
                            //        catch (Exception e)
                            //        {
                            //            Console.WriteLine("Error when sending a message \r\n {0}", e.ToString());
                            //            Statistics["Errors"].Track(DateTime.Now);
                            //        }
                            //    }
                            //}


                            //Console.WriteLine("\r\n[{0}]\r\nChat ID: {1}", DateTime.Now.ToString(), ID);
                            //Console.WriteLine("Update type = {0},\r\nInput = {1}", update.Type.ToString(), Input.Trim());
                            //Console.WriteLine("Reply = \r\n{0}", Reply.Trim());

                            IReplyMarkup Keyboard = inlineKeyboard;
                            Keyboard = Keyboard ?? replyKeyboard;
                            SendMessage(Sessions.Find(x => x.ID == ID), Reply, String.Format("Update type = {0},\r\nInput = {1}", updType.ToString(), Input.Trim()), Keyboard);


                            offset = update.Id + 1;

                            Statistics["Updates"].Track(DateTime.Now); 
                            Statistics["UpdatesLast20Minutes"].Track(DateTime.Now, Statistics["Updates"].GetTotalDynamic(DateTime.Now.AddMinutes(-20), DateTime.Now));
                            Statistics["AverageUpdatesLast20Minutes"].Track(DateTime.Now, Statistics["UpdatesLast20Minutes"].GetAverageDynamic(DateTime.Now.AddMinutes(-120), DateTime.Now));
                            Statistics["UpdatesIncremental"].TrackIncremental(DateTime.Now);
                        }

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("\r\n=++====================================++=");
                        Console.WriteLine("Exception caught");
                        Console.WriteLine(ex.ToString());
                        Console.WriteLine("==========================================");
                        Console.WriteLine("ChatID = {0}", ID);
                        Console.WriteLine("Input = {0}", Input);
                        Console.WriteLine("=--====================================--=");
                        crashed = true;
                        Statistics["Errors"].Track(DateTime.Now);
                    }

                    if (SessionPurgeTimer++ > SessionPurgeTimerInterval)
                    {
                        SessionPurgeTimer = 0;
                        long memory = GC.GetTotalMemory(true);
                        Statistics["Memory"].Track(DateTime.Now, (int)(memory / 1024));
                        if (memory > MaxAllowedMemory)
                        {
                            Console.WriteLine("Starting session purge, currently {0} sessions active", Sessions.Count);
                            Console.WriteLine("Program is using {0} bytes", memory);
                            int PurgeEntry = offset - MaxSessions;
                            Sessions.RemoveAll(x => x.age < PurgeEntry);
                            Console.WriteLine("Purge finished, {0} sessions left", Sessions.Count);
                            Console.WriteLine("Program is using {0} bytes", GC.GetTotalMemory(true));
                        }
                    }

                    if (DateTime.Now - LastStatsReset > MaxStatsKeepTimespan)
                    {
                        string path = String.Format(@"{0}/{1}", CommonConstants.FullPath, CommonConstants.StatisticsFile);
                        File.AppendAllText(path, String.Format("{1}{0}{1}", DateTime.Now.ToString(), Environment.NewLine));
                        File.AppendAllText(path, String.Format("{0} today {1}{2}", "Updates", Statistics["Updates"].GetTotal(DateTime.Now.AddDays(-1), DateTime.Now, true), Environment.NewLine));
                        File.AppendAllText(path, String.Format("{0} today {1}{2}", "Messages sent", Statistics["Messages"].GetTotal(DateTime.Now.AddDays(-1), DateTime.Now, true), Environment.NewLine));
                        File.AppendAllText(path, String.Format("{0} today {1}{2}", "Errors", Statistics["Errors"].GetTotal(DateTime.Now.AddDays(-1), DateTime.Now, true), Environment.NewLine));
                        File.AppendAllText(path, String.Format("{0} today {1}{2}", "New users", Statistics["NewUsers"].GetTotal(DateTime.Now.AddDays(-1), DateTime.Now, true), Environment.NewLine));
                        File.AppendAllText(path, "===============");
                        foreach (var metric in Statistics)
                            metric.Value.Clear(DateTime.Now.AddDays(-1));
                        LastStatsReset = DateTime.Now;
                    }

                    await Task.Delay(340);
                }
        }


        public static async Task SendMessage(Session session, string Message, string LogEntry = "", IReplyMarkup Keyboard = null, bool ignoreFormatting = false)
        {
            Keyboard = Keyboard ?? new ReplyKeyboardRemove();
            if (!String.IsNullOrWhiteSpace(Message))
            {
                try
                {
                    if (!ignoreFormatting) Message = Helper.CleanupHTMLMessage(Message);
                    Telegram.Bot.Types.Message msg;
                    if (Message == CommonConstants.EndOfFileFlag) Message = session.locale.GetMessageString(CommonConstants.MessageEndOfFile);
                    if (Message.Contains(CommonConstants.IsPictureFlag))
                    {
                        string path = CommonConstants.FullPath + Message.Replace(CommonConstants.IsPictureFlag, "");
                        msg = await Bot.SendPhotoAsync(session.ID, new FileToSend(path, File.Open(path, FileMode.Open)), replyMarkup: Keyboard);
                    }
                    else
                        msg = await Bot.SendTextMessageAsync(session.ID, Message, replyMarkup: Keyboard, parseMode: !ignoreFormatting ? ParseMode.Html : ParseMode.Default);
                    try { if (session.lastMessageID != -1) await Bot.EditMessageReplyMarkupAsync(session.ID, session.lastMessageID, null); } catch { }
                    session.lastMessageID = msg.MessageId;
                    Statistics["Messages"].Track(DateTime.Now);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error when sending a message \r\n {0}", e.ToString());
                    Statistics["Errors"].Track(DateTime.Now);
                    Statistics["TelegramErrors"].Track(DateTime.Now);
                    if (e.ToString().Contains("Telegram.Bot.Exceptions.ApiRequestException: Forbidden")) session.StopAutoscroll();
                }
            }

            Console.WriteLine("\r\n[{0}]\r\nChat ID: {1}", DateTime.Now.ToString(), session.ID);
            Console.WriteLine(LogEntry);
            Console.WriteLine("Reply : \r\n{0}", Message.Trim());
        }



    }

    
}
