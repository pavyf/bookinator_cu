﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using Telegram.Bot.Types.InlineKeyboardButtons;
using Telegram.Bot.Types.Enums;
using System.Text.RegularExpressions;

namespace Telegram.Bot.Bookinator
{
    static class Helper
    {
        public static InlineKeyboardButton[][] GetInlineKeyboard(int CurrentPage, int MaxPages, bool IsReadingNote, Localization locale, bool isAutoscroll = false)
        {
            //List<InlineKeyboardButton> Keyboard = new List<InlineKeyboardButton>();
            //Keyboard.Add(new InlineKeyboardButton(String.Format("{0}/{1}", CurrentPage, MaxPages), CommonConstants.CallbackQueryMenu));
            //if (IsReadingNote) Keyboard.Add(new InlineKeyboardButton(locale.GetButtonString(CommonConstants.ButtonSkipNote), CommonConstants.CallbackQuerySkipNote));
            //if (isAutoscroll)
            //{
            //    Keyboard.Add(new InlineKeyboardButton(locale.GetButtonString(CommonConstants.ButtonSlower), CommonConstants.CallbackQuerySlower));
            //    Keyboard.Add(new InlineKeyboardButton(locale.GetButtonString(CommonConstants.ButtonFaster), CommonConstants.CallbackQueryFaster));
            //    return new[] { Keyboard.ToArray(), new InlineKeyboardButton[] { new InlineKeyboardButton(locale.GetButtonString(CommonConstants.ButtonStopAutoscroll), CommonConstants.CallbackQueryToggleAutoscroll) } };
            //}
            //else
            //{
            //    Keyboard.Add(new InlineKeyboardButton(locale.GetButtonString(CommonConstants.ButtonStartAutoscroll), CommonConstants.CallbackQueryToggleAutoscroll));
            //    return new[] { Keyboard.ToArray(), new InlineKeyboardButton[] { new InlineKeyboardButton(locale.GetButtonString(CommonConstants.ButtonContinue), CommonConstants.CallbackQueryContinue) } };
            //}

            List<InlineKeyboardButton> Keyboard = new List<InlineKeyboardButton>();
            Keyboard.Add(InlineKeyboardButton.WithCallbackData(String.Format("{0}/{1}", CurrentPage, MaxPages), CommonConstants.CallbackQueryMenu));
            if (IsReadingNote) Keyboard.Add(InlineKeyboardButton.WithCallbackData(locale.GetButtonString(CommonConstants.ButtonSkipNote), CommonConstants.CallbackQuerySkipNote));
            Keyboard.Add(InlineKeyboardButton.WithCallbackData(locale.GetButtonString(CommonConstants.ButtonContinue), CommonConstants.CallbackQueryContinue));
            return new[] { Keyboard.ToArray() };
        }

        public static string GetChatName(Chat Chat)
        {
            switch (Chat.Type)
            {
                case ChatType.Channel: { return Chat.Title; }
                case ChatType.Group: { return Chat.Title; }
                case ChatType.Private: { return String.Format("{0} : {1} {2}", Chat.Username, Chat.FirstName, Chat.LastName); }
                default: { return ""; }
            }
        }

        public static ReplyMarkup GetReplyKeyboard(List<string> data)
        {
            List<KeyboardButton[]> buttons = new List<KeyboardButton[]>();
            int i = 0;
            foreach (string line in data)
                buttons.Add(new KeyboardButton[] { new KeyboardButton(++i + ". " + line) });
            buttons.Add(new KeyboardButton[] { new KeyboardButton(CommonConstants.CommandCancel) });
            ReplyMarkup kb = new ReplyKeyboardMarkup(buttons.ToArray(), oneTimeKeyboard: true);
            kb.Selective = true;
            return kb;
        }

        public static ReplyMarkup GetAutoscrollKeyboard()
        {
            List<KeyboardButton[]> buttons = new List<KeyboardButton[]>();
            buttons.Add(new KeyboardButton[] { new KeyboardButton(CommonConstants.CommandSlowerAutoscroll), new KeyboardButton(CommonConstants.CommandFasterAutoscroll), new KeyboardButton(CommonConstants.CommandStopAutoscroll) });
            ReplyMarkup kb = new ReplyKeyboardMarkup(buttons.ToArray(), resizeKeyboard: true, oneTimeKeyboard: false);
            kb.Selective = true;
            return kb;
        }

        public static int parseLibraryLineToIndex(string line, List<string> Library)
        {
            Regex rx = new Regex(@"^\d+\. ");
            if (rx.IsMatch(line))
            {
                int index = Int32.Parse(rx.Match(line).Value.Replace(". ", "")) - 1;
                return (Library[index] == line.Replace(rx.Match(line).Value, "")) ? index : -1;
            }
            else
                return -1;
        }

        public static InlineKeyboardButton[] GetBookOptionsKeyboard(string BookID, Localization locale)
        {
            List<InlineKeyboardButton> Keyboard = new List<InlineKeyboardButton>();
            Keyboard.Add(InlineKeyboardButton.WithCallbackData(locale.GetButtonString(CommonConstants.ButtonRead), CommonConstants.CallbackQueryReadBook + BookID));
            Keyboard.Add(InlineKeyboardButton.WithCallbackData(locale.GetButtonString(CommonConstants.ButtonDelete), CommonConstants.CallbackQueryDeleteBook + BookID));
            Keyboard.Add(InlineKeyboardButton.WithCallbackData(locale.GetButtonString(CommonConstants.ButtonCancel), CommonConstants.CallbackQueryCancel));
            return Keyboard.ToArray();
        }

        public static InlineKeyboardButton[] GetConfirmKeyboard(string YesInfo = "", Localization locale = null)
        {
            List<InlineKeyboardButton> Keyboard = new List<InlineKeyboardButton>();
            Keyboard.Add(InlineKeyboardButton.WithCallbackData(locale.GetButtonString(CommonConstants.ButtonYes), CommonConstants.CallbackQueryConfirm + YesInfo));
            Keyboard.Add(InlineKeyboardButton.WithCallbackData(locale.GetButtonString(CommonConstants.ButtonNo), CommonConstants.CallbackQueryCancel));
            return Keyboard.ToArray();
        }


        public static string CleanupHTMLMessage(string input)
        {
            input = input.Replace("&", "&amp;");
            input = Regex.Replace(input, "<(?!/*(" + String.Join("|", CommonConstants.AvailableTags) + ")>)", "&lt;");
            input = Regex.Replace(input, "(?<!</*(" + String.Join("|", CommonConstants.AvailableTags) + "))>", "&gt;");
            return input;
        }

        public static string[] ParseTelegramCommand(ref string input, string BotUsername)
        {
            input = input.ToLower();
            string[] parts = Regex.Split(input, @"\s");
            input = parts[0];
            if (Regex.IsMatch(input, @"\A(" + String.Join("|", CommonConstants.CommandNames) + ")(@" + BotUsername + @")*\z")) // decided to ignore '/' as commands don't have to start with it
            {
                input = input/*.Substring(1)*/.Replace("@" + BotUsername, String.Empty);
                return parts/*.Skip(1)*/.Where(x => !String.IsNullOrWhiteSpace(x)).ToArray();
            }
            else
                return new string[] { };
        }

        public static void HandleUpdate(string input, Session session, string BotUsername)
        {
            string[] options = ParseTelegramCommand(ref input, BotUsername);
            ICommandHandler handler = CommonConstants.CommandHandlers.ChatStateHandlers(session.state).Find(x => Regex.IsMatch(input, @"\A" + x.Name + @"\z"));
            if (handler != null)
                handler.Execute(session, options);
            else
                CommonConstants.DefaultHandler.Execute(session, options);
        }
    }
}
