﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using FB2Library;
using System.Xml;
using System.Xml.Linq;
using FB2Library.Elements;
using System.Security.Cryptography;
//using eBdb.EpubReader;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Threading;
using System.Management;
using VersFx.Formats.Text.Epub;

namespace Telegram.Bot.Bookinator
{
    class Book
    {
        private List<string> _lines = new List<string>();
        //private List<KeyValuePair<int, string>> _contents = new List<KeyValuePair<int, string>>();
        private List<string> _contents = new List<string>();
        private List<NoteItem> _notes = new List<NoteItem>();
        public List<string> Lines { get { return _lines; } }
        public List<string> Contents { get { return _contents; } }
        public List<NoteItem> Notes { get { return _notes; } }
        private string _author = string.Empty;
        private string _title = string.Empty;
        public string Author { get { return _author; } }
        public string Title { get {return _title; } }
        public string id;
        public bool broken = false;
        public string cover = string.Empty;

        public Book(string Filename, CommonConstants.Filetype type = CommonConstants.Filetype.TXT)
        {
            if (type == CommonConstants.Filetype.BOOK) id = Path.GetFileNameWithoutExtension(Filename);
            Parse(new FileStream(Filename, FileMode.Open), type);
        }
        
        public Book(Stream Filename, CommonConstants.Filetype type = CommonConstants.Filetype.TXT)
        {
            Parse(Filename, type);
        }

        private void Parse(Stream fileStream, CommonConstants.Filetype type)
        {
            _lines = new List<string>();
            try
            {
                if (type != CommonConstants.Filetype.BOOK) id = Guid.NewGuid().ToString();

                switch (type)
                {
                    case CommonConstants.Filetype.BOOK:
                        {
                            ParseBOOK(fileStream);
                            break;
                        }
                    case CommonConstants.Filetype.TXT:
                        {
                            ParseTXT(fileStream);
                            break;
                        }
                    case CommonConstants.Filetype.FB2:
                        {
                            ParseFB2(fileStream);
                            break;
                        }
                    case CommonConstants.Filetype.EPUB:
                        {
                            ParseEPUB(fileStream);
                            break;
                        }
                    case CommonConstants.Filetype.MOBI:
                        {
                            ParseMOBI(fileStream);
                            break;
                        }
                }

                if (type != CommonConstants.Filetype.BOOK) splitTextFurther();
                _contents = FixContentPage();

                List<string> newLines = new List<string>();
                foreach (string line in _lines)
                {
                    newLines.Add(CompleteTags(line));
                    _lines = newLines;
                }

                if (type != CommonConstants.Filetype.BOOK)
                { 
                    Save();
                }
            }
            catch (Exception e) { broken = true; Console.WriteLine("Error while parsing a book\r\n{0}\r\n", e.ToString()); }
        }

        private void ParseBOOK(Stream fileStream)
        {
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            settings.IgnoreComments = true;
            XmlReader reader = XmlReader.Create(fileStream, settings);
            string Name = "";
            string Value = "";
            string Attribute = "";
            bool isNote = false;
            List<string> newNote = new List<string>();
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        Name = reader.Name;
                        if (Name == "note_item")
                        {
                            isNote = true;
                            reader.MoveToAttribute("id");
                            Attribute = reader.Value;
                        }
                        break;
                    case XmlNodeType.Text:
                        Value = reader.Value;
                        break;
                    case XmlNodeType.XmlDeclaration:
                        break;
                    case XmlNodeType.ProcessingInstruction:
                        break;
                    case XmlNodeType.Comment:
                        break;
                    case XmlNodeType.Attribute:
                        if (reader.Name == "id") Attribute = reader.Value;
                        break;
                    case XmlNodeType.EndElement:
                        Name = reader.Name;
                        switch (Name)
                        {
                            case "Author":
                                _author = Value;
                                break;
                            case "Title":
                                _title = Value;
                                break;
                            case "Cover":
                                cover = Value;
                                break;
                            case "content_item":
                                _contents.Add(Value);
                                break;
                            case "line":
                                if (isNote)
                                    newNote.Add(Value);
                                else
                                    _lines.Add(Value);
                                break;
                            case "note_item":
                                isNote = false;
                                NoteItem Note = new NoteItem(Attribute);
                                Note.Content.AddRange(newNote);
                                _notes.Add(Note);
                                newNote.Clear();
                                break;
                        }
                        break;
                }
            }
            reader.Close();
            reader.Dispose();


            //XmlDocument doc = new XmlDocument();
            //doc.Load(fileStream);
            //XmlElement root = doc.DocumentElement;
            //foreach (XmlNode node in root.ChildNodes)
            //{
            //    switch (node.Name)
            //    {
            //        case "Author":
            //            {
            //                _author = node.InnerText;
            //                break;
            //            }
            //        case "Title":
            //            {
            //                _title = node.InnerText;
            //                break;
            //            }
            //        case "Cover":
            //            {
            //                cover = node.InnerText;
            //                break;
            //            }
            //        case "line":
            //            {
            //                _lines.Add(node.InnerText);
            //                break;
            //            }
            //        case "content_item":
            //            {
            //                //_contents.Add(new KeyValuePair<int, string>(node.Attributes["id"].InnerText));
            //                _contents.Add(node.InnerText);
            //                break;
            //            }
            //        case "note_item":
            //            {
            //                NoteItem newNote = new NoteItem(node.Attributes["id"].InnerText);
            //                foreach (XmlNode noteLine in node)
            //                {
            //                    switch (noteLine.Name)
            //                    {
            //                        case "line":
            //                            {
            //                                newNote.Content.Add(noteLine.InnerText);
            //                                break;
            //                            }
            //                    }
            //                }
            //                _notes.Add(newNote);
            //                break;
            //            }
            //    }
            //}
        }

        private void ParseFB2(Stream fileStream)
        {
            FB2File file = new FB2File();

            XDocument doc = XDocument.Load(fileStream);

            file.Load(doc, false);
            try
            {
                //_author = file.DocumentInfo.DocumentAuthors[0].FirstName + " " + file.DocumentInfo.DocumentAuthors[0].LastName;
                _author = file.TitleInfo.BookAuthors.First().FirstName + " " + file.TitleInfo.BookAuthors.First().LastName;
                _title = file.TitleInfo.BookTitle.Text;
                cover = "\\Books\\" + id + "\\" + file.TitleInfo.Cover.CoverpageImages[0].HRef.Remove(0, 1);
            }
            catch (Exception e) { }
            foreach (BodyItem body in file.Bodies)
            {
                if (body.Name == "notes" || body.Name == "comments")
                    foreach (SectionItem section in body.Sections)
                    {
                        NoteItem newNote = new NoteItem(section.ID);
                        newNote.Content.AddRange(ParseFB2Section(section, IsNote: true));
                        _notes.Add(newNote);
                    }
                else
                    foreach (SectionItem section in body.Sections)
                        _lines.AddRange(ParseFB2Section(section));
            }
            Dictionary<string, BinaryItem> images = file.Images;
            foreach (KeyValuePair<string, BinaryItem> pic in images)
            {
                string path = CommonConstants.FullPath + "\\Books\\" + id;
                Directory.CreateDirectory(path);
                File.WriteAllBytes(path + "/" + pic.Key, pic.Value.BinaryData);
            }
        }

        /// <summary>
        /// Рекурсивно парсит секции с учетом некоторых важных тэгов
        /// </summary>
        /// <param name="section">Нода, в которой мы сейчас находимся</param>
        /// <param name="depth">Глубина рекурсии</param>
        /// <param name="lines">Считат сколько было строк для последующего формирования оглавления</param>
        /// <returns>Возвращяет распаршеный параграф с форматированием в списке строк</returns>
        private List<string> ParseFB2Section(SectionItem section, int depth = 0, int lines = 0, bool IsNote = false)
        {
            List<string> text = new List<string>();
            if ((section.Title != null) && !IsNote)
            {
                foreach (IFb2TextItem title in section.Title.TitleData)
                {
                    if (!String.IsNullOrWhiteSpace(title.ToString()))
                    {
                        text.Add(String.Format("<b>{0}</b>", title.ToString().Trim()));
                        string s = "";
                        for (int i = 0; i < depth; i++) s += "  ";
                        _contents.Add(String.Format("{0}{1} — /{2}", s, title.ToString().Trim(), _lines.Count + text.Count + lines));
                        //_contents.Add(new KeyValuePair<int, string>(_lines.Count + text.Count + lines, s + title.ToString().Trim()));
                    }
                }
            }
            foreach (EpigraphItem epigraph in section.Epigraphs)
            {
                foreach (IFb2TextItem ep in epigraph.EpigraphData)
                {
                    if (!String.IsNullOrWhiteSpace(ep.ToString()))
                        text.Add(String.Format("<i>{0}</i>", ep.ToString().Trim()));
                }
            }
            foreach (IFb2TextItem node in section.Content)
            {
                if (node.GetType() == typeof(SectionItem))
                {
                    SectionItem child = node as SectionItem;
                    text.AddRange(ParseFB2Section(child, depth + 1, lines + text.Count, IsNote));
                }
                else if (node.GetType() == typeof(CiteItem))
                {
                    CiteItem cite = node as CiteItem;
                    foreach (IFb2TextItem data in cite.CiteData)
                        if (!String.IsNullOrWhiteSpace(data.ToString()))
                            text.Add(String.Format("<i>&quot;{0}&quot;</i>", data.ToString().Trim()));
                }
                else if (node.GetType() == typeof(ImageItem))
                {
                    ImageItem Image = node as ImageItem;
                    text.Add(CommonConstants.IsPictureFlag + "\\Books\\" + id + "\\" + Image.HRef.Remove(0, 1));
                }
                else if (node.GetType() == typeof(SimpleText))
                {
                    SimpleText data = node as SimpleText;
                    if (!String.IsNullOrWhiteSpace(data.Text)) text.Add(data.Text);
                }
                else if (node.GetType() == typeof(ParagraphItem))
                {
                    ParagraphItem child = node as ParagraphItem;
                    text.AddRange(ParseFB2Paragraph(child));
                }

                // сюда надо напихать других тэгов. Есть, например, тэги со стихами, их надо напихать

            }
            return text;
        }

        /// <summary>
        /// Параграф представляет собой набор размеченного текста
        /// Чтобы нормально с ним работать, либа разбивает его на куски с неизменным стилем.
        /// Мы эти куски вылавливаем, смотрим стиль, добавляем разметку и склеиваем обратно
        /// </summary>
        /// <param name="Paragraph"></param>
        /// <returns></returns>
        private List<string> ParseFB2Paragraph(ParagraphItem Paragraph)
        {
            List<string> text = new List<string>();
            // вообще-то это плохо: параграфы - это одна строка, и если у нас есть много меняющихся стилей в одном, он будет разделяться на много.
            // логично просто пихать в StringBuilder, но тогда картинки полетят. Хм.
            // пока что решил картинки отделять от всего переносом строки. У этого есть проблема: если посреди отформатированного текста есть перенос, например так:
            // "<emphasis> blah-blah \r\n second line </emphasis>"
            // то форматирование полетит

            StringBuilder ParsedData = new StringBuilder();

            foreach (StyleType data in Paragraph.ParagraphData)
            {
                if (data.GetType() == typeof(InlineImageItem))
                {
                    InlineImageItem Image = data as InlineImageItem;
                    ParsedData.AppendLine();
                    ParsedData.Append(CommonConstants.IsPictureFlag + "\\Books\\" + id + "\\" + Image.HRef.Remove(0, 1));
                    ParsedData.AppendLine();
                }
                else if (data.GetType() == typeof(InternalLinkItem))
                {
                    InternalLinkItem link = data as InternalLinkItem;
                    ParsedData.Append(" /"+link.HRef.Replace(@"#", ""));
                }
                else if (data.GetType() == typeof(SimpleText))
                {
                    SimpleText line = data as SimpleText;
                    string trimmedLine = " " + line.Text.Trim();
                    if (!String.IsNullOrWhiteSpace(trimmedLine))
                        switch (line.Style)
                        {
                            case TextStyles.Normal:
                                {
                                    ParsedData.Append(trimmedLine);
                                    break;
                                }
                            case TextStyles.Strong:
                                {
                                    ParsedData.Append(String.Format("<b>{0}</b>", trimmedLine));
                                    break;
                                }
                            case TextStyles.Emphasis:
                                {
                                    ParsedData.Append(String.Format("<i>{0}</i>", trimmedLine));
                                    break;
                                }

                            // здесь надо может добавить стилей, я хз

                            default: 
                                {
                                    ParsedData.Append(trimmedLine);
                                    break;
                                }
                        
                        }
                }

                // сюда тоже надо напихать других тэгов

            }

            text = ParsedData.ToString().Trim().Split(new[] { "\n" }, StringSplitOptions.RemoveEmptyEntries).ToList<String>();
            return text;
        }

        private void ParseTXT(Stream fileStream)
        {
            string[] file;
            using (StreamReader sr = new StreamReader(fileStream))
                file = sr.ReadToEnd().Trim().Split(new[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);
            //string s = "";
            //foreach (string line in file)
            //{
            //    if (s.Length + line.Length < 400)
            //    {
            //        s = s + line + "\r\n";
            //    }
            //    else
            //    {
            //        _lines.Add(s);
            //        s = line;
            //    }
            //}
            //_lines.Add(s);
            foreach (string line in file) _lines.Add(line.Trim());
            _lines.RemoveAll(x => String.IsNullOrWhiteSpace(x));
        }

        private void ParseEPUB(Stream fileStream)
        {
            //StreamReader sr = new StreamReader(fileStream);
            //string content = sr.ReadToEnd();
            byte[] content = ReadFully(fileStream);
            string path = @"C:\"+Guid.NewGuid().ToString() + ".epub";
            File.WriteAllBytes(path, content);

            //EpubBook epubBook = EpubReader.ReadBook(path);
            ParseEPUBContentPage(EpubReader.ReadBook(path));

            string fb2File = RunCalibre2(path);
            using (FileStream stream = new FileStream(fb2File, FileMode.Open))
                ParseFB2(stream);
            File.Delete(path);
            File.Delete(fb2File);
            //Epub book = new Epub(path);
            //_author = book.Creator[0];
            //_title = book.Title[0];
            //string[] text = book.GetContentAsPlainText().Split(new[] { "\n" }, StringSplitOptions.None);
            //foreach (string line in text) if (!String.IsNullOrWhiteSpace(line)) _lines.Add(line);
            //File.Delete(path);
        }

        private void ParseEPUBContentPage(EpubBook data)
        {
            foreach (EpubChapter chapter in data.Chapters)
                ParseEPUBChapter(chapter);
        }

        private void ParseEPUBChapter(EpubChapter chapter, int depth = 1)
        {
            _contents.Add(String.Format("{0} — /{1}", chapter.Title, depth++));
            foreach (EpubChapter child in chapter.SubChapters)
                ParseEPUBChapter(child, depth++);
        }

        private void ParseMOBI(Stream fileStream)
        {
            byte[] content = ReadFully(fileStream);
            string path = @"C:\" + Guid.NewGuid().ToString() + ".mobi";
            File.WriteAllBytes(path, content);
            string fb2File = RunCalibre2(path);
            using (FileStream stream = new FileStream(fb2File, FileMode.Open))
                ParseFB2(stream);
            File.Delete(path);
            File.Delete(fb2File);
        }

        private static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        private void Save()
        {
            string path = CommonConstants.FullPath+@"\Books\" + id + ".book";
            if (File.Exists(path)) File.Delete(path);
            File.Create(path).Close();
            XmlTextWriter doc = new XmlTextWriter(path, Encoding.UTF8);
            doc.Formatting = Formatting.Indented;
            doc.WriteStartDocument();
                doc.WriteStartElement("Book");
                    doc.WriteStartElement("Author");
                        doc.WriteString(Author);
                    doc.WriteEndElement();
                    doc.WriteStartElement("Title");
                        doc.WriteString(Title);
                    doc.WriteEndElement();
                    doc.WriteStartElement("Cover");
                        doc.WriteString(cover);
                    doc.WriteEndElement();
                    foreach (string line in _lines)
                    {
                        doc.WriteStartElement("line");
                            doc.WriteString(line);
                        doc.WriteEndElement();
                    }
                    //foreach (KeyValuePair<int, string> Title in _contents)
                    //{
                    //    doc.WriteStartElement("content_item");
                    //        doc.WriteAttributeString("position", "", Title.Key.ToString());
                    //        doc.WriteString(Title.Value);
                    //    doc.WriteEndElement();
                    //}
                    foreach (string line in _contents)
                    {
                        doc.WriteStartElement("content_item");
                            doc.WriteString(line);
                        doc.WriteEndElement();
                    }
                    foreach (NoteItem note in _notes)
                    {
                        doc.WriteStartElement("note_item");
                        doc.WriteAttributeString("id", "", note.ID);
                        foreach (string noteLine in note.Content)
                        {
                            doc.WriteStartElement("line");
                              doc.WriteString(noteLine);
                            doc.WriteEndElement();
                        }
                        doc.WriteEndElement();
                    }
            doc.WriteEndElement();
            doc.WriteEndDocument();
            doc.Close();
        }

        public string getName()
        {
            return String.Format("{0}{1}{2}", _author, !(String.IsNullOrWhiteSpace(_author))&&!(String.IsNullOrWhiteSpace(_title))?", ":"", _title);
        }

        public void setName(string newName)
        {
            _author = "";
            _title = newName;
            this.Save();
        }

        private void splitTextFurther()
        {
            List<string> _newLines = new List<string>();

            foreach (string line in _lines)
            {
                string[] sentences = Regex.Split(line, @"(?<=[.!?])\s+");

                string newLine = "";
                int counter = 0;
                int sentencesCount = sentences.Length;  

                foreach (string sentence in sentences)
                {
                    counter += 1;
                    var length = sentence.Length;
                    if (sentence.Length > 400)
                    {
                        if (!String.IsNullOrWhiteSpace(newLine)) _newLines.Add(newLine);
                        _newLines.Add(sentence);
                        newLine = "";
                        continue;
                    }

                    if (newLine.Length + length <= 400)
                    {
                        newLine += " " + sentence;
                        if (counter == sentencesCount)
                        {
                            if (!String.IsNullOrWhiteSpace(newLine)) _newLines.Add(newLine);
                        }
                    }
                    else
                    {
                        _newLines.Add(newLine);
                        newLine = sentence;
                        if ((counter == sentencesCount) && !String.IsNullOrWhiteSpace(sentence))
                        {
                            //newLine += sentence;
                            _newLines.Add(sentence);
                        }
                    }
                }
            }

            _lines = _newLines;
        }

        private string CompleteTags(string input)
        {
            return CompleteTags(input, CommonConstants.AvailableTags);
        }

        private string CompleteTags(string input, string[] tags)
        {
            if (!String.IsNullOrEmpty(input))
            {
                foreach (string tag in tags)
                {
                    if ((input.IndexOf(String.Format("<{0}>", tag)) == -1) && (input.IndexOf(String.Format("</{0}>", tag)) != -1))
                        input = String.Format("<{0}>{1}", tag, input);
                    if ((input.IndexOf(String.Format("<{0}>", tag)) != -1) && (input.IndexOf(String.Format("</{0}>", tag)) == -1))
                        input = String.Format("{1}</{0}>", tag, input);
                }
            }
            return input;
        }

        private string RunCalibre2(string FilePath)
        {
            Console.WriteLine("[RunCalibre2] started. Filepath: {0}", FilePath);

            string outputFile =@"C:\" + Guid.NewGuid().ToString() + ".fb2";
            var command = String.Format("\"{0} {1} {2}\"", CommonConstants.Calibre2Executable, FilePath, outputFile);
            Console.WriteLine("Full command: {0}", command);
            int time = CommonConstants.ConverterTime;
            int kill = CommonConstants.ConverterKill;
            int maxMem = CommonConstants.ConverterMaxMem;
            string output;
            int procId = 0;

            try
            {
                var cmdStartInfo = new ProcessStartInfo("cmd", "/c " + command)
                {
                    RedirectStandardError = true,
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    WorkingDirectory = CommonConstants.Calibre2Path
                };

                using (var cmdProcess = new Process())
                {
                    cmdProcess.StartInfo = cmdStartInfo;
                    cmdProcess.Start();

                    try
                    {
                        procId = cmdProcess.Id;
                    }

                    catch (InvalidOperationException) { }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.StackTrace);
                        throw;
                    }

                    if (maxMem != -1)
                    {
                        ThreadStart threadMain = delegate { SizeControl(maxMem, cmdProcess, procId); };
                        new Thread(threadMain).Start();
                    }


                    if (kill == 1 && time > 0)
                    {
                        if (!cmdProcess.WaitForExit(time))
                            KillProcessAndChildren(procId);

                        //cmdProcess.Kill();
                    }


                    output = cmdProcess.StandardOutput.ReadToEnd();

                    if (string.IsNullOrEmpty(output))
                        output = cmdProcess.StandardError.ReadToEnd();

                }

            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                outputFile = "";
                throw;
            }

            return outputFile;
        }

        static void SizeControl(int maxMem, Process cmdProcess, int procId)
        {
            try
            {
                while (!cmdProcess.HasExited)
                {
                    var totalBytesOfMemoryUsed = cmdProcess.WorkingSet64;
                    if (totalBytesOfMemoryUsed > maxMem)
                        KillProcessAndChildren(procId);
                    Thread.Sleep(2500);
                    cmdProcess.Refresh();
                }
                Thread.CurrentThread.Abort();
            }
            catch (Exception)
            {
                // Process already exited.
            }

        }

        private static void KillProcessAndChildren(int pid)
        {
            var searcher = new ManagementObjectSearcher
                ("Select * From Win32_Process Where ParentProcessID=" + pid);
            var moc = searcher.Get();
            foreach (var o in moc)
            {
                if (!o.GetType().IsAssignableFrom(typeof(ManagementObject)))
                    continue;
                var mo = (ManagementObject)o;
                KillProcessAndChildren(Convert.ToInt32(mo["ProcessID"]));
            }
            try
            {
                var proc = Process.GetProcessById(pid);
                if (!proc.HasExited)
                    proc.Kill();
            }
            catch (Exception)
            {
                // Process already exited.
            }
        }

        public bool IsNote(string id)
        {
            return (_notes.Find(x => x.ID == id) != null);
        }

        private KeyValuePair<int, string> ParseTitle(string title)
        {
            Regex rx = new Regex(@"\d{1,}$");
            string s = rx.Match(title).Value;
            return new KeyValuePair<int, string>(Int32.Parse(s), title.Replace(" — /"+s, String.Empty));
        }

        private List<string> FixContentPage()
        {
            List<string> newContentPage = new List<string>();
            if (_contents.Count > 0)
            {
                List<KeyValuePair<int, string>> ParsedContentPage = new List<KeyValuePair<int, string>>();
                foreach (string titleString in _contents) ParsedContentPage.Add(ParseTitle(titleString));
                int Counter = 0;
                int LineIndex = 0;
                foreach (string line in _lines)
                {
                    if (Counter + 1 == _contents.Count) break;
                    //string s = line.Replace("<b>", String.Empty).Replace("</b>", String.Empty).Replace(ParsedContentPage[Counter].Value, String.Empty);
                    string s1 = line.Replace("<b>", String.Empty).Replace("</b>", String.Empty);
                    s1 = Regex.Replace(s1, @"[^\w]", String.Empty);
                    //string s2 = ParsedContentPage[Counter].Value;
                    //s2 = Regex.Replace(s2, @"[^\w]", String.Empty);
                    ////if (line.Replace(" ", String.Empty).Equals(String.Format("<b>{0}</b>", ParsedContentPage[Counter].Value.Replace(" ", String.Empty))))
                    ////if (String.IsNullOrWhiteSpace(line.Replace("<b>", String.Empty).Replace("</b>", String.Empty).Replace(ParsedContentPage[Counter].Value, String.Empty)))
                    //if (s1.Equals(s2, StringComparison.OrdinalIgnoreCase))
                    //{
                    //    newContentPage.Add(String.Format("{0} — /{1}", ParsedContentPage[Counter].Value, LineIndex + 1));
                    //    Counter++;
                    //}

                    foreach (KeyValuePair<int, string> bwah in ParsedContentPage)
                    {
                        string s2 = bwah.Value;
                        s2 = Regex.Replace(s2, @"[^\w]", String.Empty);
                        if (s1.Equals(s2, StringComparison.OrdinalIgnoreCase))
                            if (newContentPage.FindLastIndex(x => x == String.Format("{0} — /{1}", bwah.Value, LineIndex + 1)) == -1) newContentPage.Add(String.Format("{0} — /{1}", bwah.Value, LineIndex + 1));
                    }
                    LineIndex++;
                }
            }
            return newContentPage;
        }

        public static string GetBookName(string ID)
        {
            string name = string.Empty;
            Stream fileStream = new FileStream(string.Format(@"{0}\Books\{1}.book", CommonConstants.FullPath, ID), FileMode.Open);
            XmlReader reader = XmlReader.Create(fileStream);
                reader.ReadToFollowing("Author");
                name = reader.ReadElementContentAsString();
                reader.ReadToFollowing("Title");
                name = (String.IsNullOrWhiteSpace(name) ? string.Empty : name + ": ") + reader.ReadElementContentAsString();
            reader.Close();
            reader.Dispose();
            return name;
        }
    }
}
