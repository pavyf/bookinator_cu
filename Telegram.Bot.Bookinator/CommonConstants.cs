﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.IO;

namespace Telegram.Bot.Bookinator
{
    public struct Constant
    {
        public string Name;
        public string Value;
    }
    static class CommonConstants
    {

        #region commands
        #region public commands

        public const string CommandStart = "/start";
        public const string CommandContinue = "/continue";
        public const string CommandReset = "/reset";
        public const string CommandLanguage = "/language";
        public const string CommandHelp = "/help";
        public const string CommandFeedback = "/feedback";
        public const string CommandLibrary = "/library";
        public const string CommandAutoscroll = "/autoscroll";

        #endregion
        #region hidden commands

        public const string CommandCancel = "/cancel";
        public const string CommandRussian = "/russian";
        public const string CommandEnglish = "/english";
        public const string CommandEditBook = "/edit";
        public const string CommandStopAutoscroll = "⏹ stop";
        public const string CommandFasterAutoscroll = "⏫ faster";
        public const string CommandSlowerAutoscroll = "⏬ slower";

        public const string CommandDebug = "/bkntr_debug";
        public const string CommandStatus = "/status";
        public const string CommandUpdates = "/updates";
        public const string CommandMemory = "/memory";

        #endregion
        #region command references

        public const string CommandReferenceStart = "cmd_start";
        public const string CommandReferenceContinue = "cmd_continue";
        public const string CommandReferenceReset = "cmd_reset";
        public const string CommandReferenceLanguage = "cmd_language";
        public const string CommandReferenceHelp = "cmd_help";
        public const string CommandReferenceFeedback = "cmd_feedback";
        public const string CommandReferenceLibrary = "cmd_library";
        public const string CommandReferenceAutoscroll = "cmd_autoscroll";

        public const string CommandReferenceCancel = "cmd_cancel";
        public const string CommandReferenceRussian = "cmd_rus";
        public const string CommandReferenceEnglish = "cmd_eng";
        public const string CommandReferenceEditBook = "cmd_edit";

        #endregion
        #endregion
        #region language symbols

        public const string LanguageSymbolEnglish = "en-EN";
        public const string LanguageSymbolRussian = "ru-RU";

        #endregion
        #region callback queries

        public const string CallbackQueryContinue = "callback_continue";
        public const string CallbackQueryReset = "callback_reset";
        public const string CallbackQueryMenu = "callback_menu";
        public const string CallbackQuerySkipNote = "callback_skip_note";
        public const string CallbackQueryDeleteBook = "callback_delete_book";
        public const string CallbackQueryCancel = "callback_cancel";
        public const string CallbackQueryReadBook = "callback_read_book";
        public const string CallbackQueryReadFromStart = "callback_read_from_start";
        public const string CallbackQueryConfirm = "callback_confirm";
        public const string CallbackQueryToggleAutoscroll = "callback_autoscroll";
        public const string CallbackQueryFaster = "callback_faster";
        public const string CallbackQuerySlower = "callback_slower";

        #endregion
        #region flags

        public const string EndOfFileFlag = "f8b17c08a25375d152bf5fc28f8e4bfe";
        public const string IsPictureFlag = "5456fc54c74a297ce994998c2873b370";
        public const string DeleteBookFlag = "dltbk";

        #endregion
        #region localization constants
        #region messages

        public const string MessageGreetings = "msg_greetings";
        public const string MessageError = "msg_error";
        public const string MessageEndOfFile = "msg_eof";
        public const string MessageHelp = "msg_help";
        public const string MessageLanguage = "msg_language";
        public const string MessageNewBook = "msg_newbook";
        public const string MessageDefault = "msg_default";
        public const string MessageFeedback = "msg_feedback";
        public const string MessageFeedbackAccepted = "msg_feedback_accepted";
        public const string MessageMenu = "msg_menu";
        public const string MessagePageNotFound = "msg_page_not_found";
        public const string MessageFormatNotSupported = "msg_not_supported";
        public const string MessageLibrary = "msg_library";
        public const string MessageBookEdit = "msg_book_edit";
        public const string MessageBookNameConfirmed = "msg_book_name_confirmed";
        public const string MessageNewVersion = "msg_new_version";
        public const string MessageConfirmDelete = "msg_confirm_delete";
        public const string MessageLargeLibrary = "msg_large_library";
        public const string MessageBookOptions = "msg_book_options";
        public const string MessageActionCancelled = "msg_action_cancelled";
        public const string MessageBookDeleted = "msg_book_deleted";

        #endregion
        #region localization pictures
        public const string PictureGreetings = "pic_greetings";
        #endregion
        #region localization buttons
        public const string ButtonContinue = "btn_continue";
        public const string ButtonReset = "btn_reset";
        public const string ButtonRead = "btn_read";
        public const string ButtonDelete = "btn_delete";
        public const string ButtonCancel = "btn_cancel";
        public const string ButtonYes = "btn_yes";
        public const string ButtonNo = "btn_no";
        public const string ButtonSkipNote = "btn_skip_note";
        public const string ButtonStartAutoscroll = "btn_start_autoscroll";
        public const string ButtonStopAutoscroll = "btn_stop_autoscroll";
        public const string ButtonFaster = "btn_faster";
        public const string ButtonSlower = "btn_slower";
        #endregion
        #region descriptions

        public const string DescriptionStart = "desc_start";
        public const string DescriptionContinue = "desc_continue";
        public const string DescriptionReset = "desc_reset";
        public const string DescriptionLanguage = "desc_language";
        public const string DescriptionHelp = "desc_help";
        public const string DescriptionFeedback = "desc_feedback";
        public const string DescriptionLibrary = "desc_library";
        public const string DescriptionAutoscroll = "desc_autoscroll";

        #endregion
        #region flags
        public const string LocaleFlagBookName = "%book_name%";
        #endregion
        #endregion

        #region string arrays (I think it's useless now)
        // public static Constant[] Commands = 
        // {
        //    new Constant { Name = CommandReferenceStart    , Value = CommandStart    },
        //    new Constant { Name = CommandReferenceContinue , Value = CommandContinue },
        //    new Constant { Name = CommandReferenceReset    , Value = CommandReset    },
        //    new Constant { Name = CommandReferenceLanguage , Value = CommandLanguage },
        //    new Constant { Name = CommandReferenceHelp     , Value = CommandHelp     },
        //    new Constant { Name = CommandReferenceCancel   , Value = CommandCancel   },
        //    new Constant { Name = CommandReferenceFeedback , Value = CommandFeedback },
        //    new Constant { Name = CommandReferenceRussian  , Value = CommandRussian  },
        //    new Constant { Name = CommandReferenceEnglish  , Value = CommandEnglish  }
        // };

        //public static Constant[] Messages =
        // {
        //    new Constant { Name = "cmd_start", Value = "/start"}
        //};
        #endregion

        #region filetypes
        public enum Filetype
        {
            BOOK = 0,
            TXT = 1,
            FB2 = 2,
            EPUB = 3,
            MOBI = 4
        }
        #endregion

        #region TeamChatID
        public static long[] TeamIDs = { 35846529 };
        public static long[] AdminIDs = { 186249549 /* pavyf */ , 35846529 /* sofinma*/};
        #endregion


        public static string FullPath = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
        public static string databaseName = FullPath + @"\Sessions\sessions.db";
        public static string logName = "Bookinator.log";
        public static string FeedbackFile = "Feedback.txt";
        public static string StatisticsFile = "Statistics.txt";
        public static SQLiteConnection databaseConnection = new SQLiteConnection(string.Format("Data Source={0}", databaseName));
        public static int version = 2;

        #region converter setup
        public static string Calibre2Path = @"C:\Program Files\Calibre2";
        public static string Calibre2Executable = @"ebook-convert.exe";
        public static int ConverterTime = 5000;
        public static int ConverterKill = 0;
        public static int ConverterMaxMem = 4000000;
        #endregion

        public static string[] AvailableTags = new[] { "i", "b" };

        //public static Book defaultBookEn = new Book(string.Format(@"{0}\Books\{1}.book", CommonConstants.FullPath, "Default_en"), CommonConstants.Filetype.BOOK);
        //public static Book defaultBookRu = new Book(string.Format(@"{0}\Books\{1}.book", CommonConstants.FullPath, "Default_ru"), CommonConstants.Filetype.BOOK);

        #region localizations
        public static List<Localization> localizations = new[] { new Localization(LanguageSymbolRussian), new Localization(LanguageSymbolEnglish) }.ToList();
        #endregion


        public static CommandList CommandHandlers = new CommandList();
        public static ICommandHandler DefaultHandler = new CommandHandlers.DefaultHandler();
        public static List<string> CommandNames = CommandHandlers.Commands.Select(x => x.Name).ToList();

        static CommonConstants()
        {
            #region populating command handlers
            CommandHandlers.AddHandlers(new List<ICommandHandler>()
            {
                new CommandHandlers.StartHandler(),
                new CommandHandlers.ContinueHandler(),
                new CommandHandlers.PageJumpHandler()
            }, Session.ChatState.Commands);

            CommandNames = CommandHandlers.Commands.Select(x => x.Name).ToList();
            #endregion
        }
    }
}
