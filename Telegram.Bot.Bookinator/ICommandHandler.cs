﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Telegram.Bot.Bookinator
{
    interface ICommandHandler
    {
        string Name { get; }
        void Execute(Session session, string[] parameters);
    }
}
