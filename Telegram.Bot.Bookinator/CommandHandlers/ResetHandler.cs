﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;

namespace Telegram.Bot.Bookinator.CommandHandlers
{
    class ResetHandler : ICommandHandler
    {
        string ICommandHandler.Name { get { return CommonConstants.CommandReset; } }
        void ICommandHandler.Execute(Session session, string[] parameters)
        {
            session.Position = 0;
            string Reply = session.GetNextLine(ResetToBook: true);
            InlineKeyboardMarkup inlineKeyboard = new InlineKeyboardMarkup(Helper.GetInlineKeyboard(session.Position, session.Book.Lines.Count, session.IsReadingNote, session.locale));
            if (Reply == CommonConstants.EndOfFileFlag) Reply = session.locale.GetMessageString(CommonConstants.MessageEndOfFile);
            Program.SendMessage(session, Reply, "Reset handler, input = '" + String.Join(" ", parameters) + "'", inlineKeyboard);
        }
    }
}
