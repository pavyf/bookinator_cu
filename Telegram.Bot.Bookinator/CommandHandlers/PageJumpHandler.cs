﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;

namespace Telegram.Bot.Bookinator.CommandHandlers
{
    class PageJumpHandler : ICommandHandler
    {
        string ICommandHandler.Name { get { return @"/(\d+)"; } }
        void ICommandHandler.Execute(Session session, string[] parameters)
        {
            string Reply = session.GetNextLine();
            int page = Int32.Parse(parameters[0].Replace("/", String.Empty));
            InlineKeyboardMarkup inlineKeyboard = null;
            if ((page > 0) && (page <= session.Book.Lines.Count))
            {
                session.Position = page - 1;
                Reply = session.GetNextLine(ResetToBook: true);
                inlineKeyboard = new InlineKeyboardMarkup(Helper.GetInlineKeyboard(session.Position, session.Book.Lines.Count, session.IsReadingNote, session.locale));
                if (Reply == CommonConstants.EndOfFileFlag) Reply = session.locale.GetMessageString(CommonConstants.MessageEndOfFile);
            }
            else
                Reply = session.locale.GetMessageString(CommonConstants.MessagePageNotFound);

            Program.SendMessage(session, Reply, "jump to page handler, input = '" + String.Join(" ", parameters) + "'", inlineKeyboard);
        }
    }
}
