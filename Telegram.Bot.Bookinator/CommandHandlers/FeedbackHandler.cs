﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;

namespace Telegram.Bot.Bookinator.CommandHandlers
{
    class FeedbackHandler : ICommandHandler
    {
        string ICommandHandler.Name { get { return CommonConstants.CommandFeedback; } }
        void ICommandHandler.Execute(Session session, string[] parameters)
        {
            session.state = Session.ChatState.Feedback;
            Program.SendMessage(session, session.locale.GetMessageString(CommonConstants.MessageFeedback), "Feedback command handler ,input = '" + String.Join(" ", parameters) + "'");
        }
    }
}