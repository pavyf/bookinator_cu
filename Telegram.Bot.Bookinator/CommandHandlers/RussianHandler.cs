﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;

namespace Telegram.Bot.Bookinator.CommandHandlers
{
    class RussianHandler : ICommandHandler
    {
        string ICommandHandler.Name { get { return CommonConstants.CommandRussian; } }
        void ICommandHandler.Execute(Session session, string[] parameters)
        {
            session.ChangeLanguage(CommonConstants.LanguageSymbolRussian);
            Program.SendMessage(session, session.locale.GetMessageString(CommonConstants.MessageGreetings), "Russian command handler ,input = '" + String.Join(" ", parameters) + "'");
            session.Update();
        }
    }
}