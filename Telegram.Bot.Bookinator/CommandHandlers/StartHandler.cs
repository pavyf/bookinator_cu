﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Telegram.Bot.Bookinator.CommandHandlers
{
    class StartHandler: ICommandHandler
    {
        string ICommandHandler.Name { get { return CommonConstants.CommandStart; } }
        void ICommandHandler.Execute(Session session, string[] parameters)
        {
            string path = session.locale.GetMessageString(CommonConstants.PictureGreetings);
            Program.SendMessage(session, String.Format("{0}{1}", CommonConstants.IsPictureFlag, path), "Welcome picture");
            Program.SendMessage(session, session.locale.GetMessageString(CommonConstants.MessageGreetings), "/start handler");
        }
    }
}
