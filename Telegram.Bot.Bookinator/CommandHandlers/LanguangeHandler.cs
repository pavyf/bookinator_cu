﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;

namespace Telegram.Bot.Bookinator.CommandHandlers
{
    class LanguageHandler : ICommandHandler
    {
        string ICommandHandler.Name { get { return CommonConstants.CommandLanguage; } }
        void ICommandHandler.Execute(Session session, string[] parameters)
        {
            Program.SendMessage(session, session.locale.GetMessageString(CommonConstants.MessageLanguage), "Language command handler ,input = '" + String.Join(" ", parameters) + "'");
        }
    }
}