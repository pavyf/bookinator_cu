﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;

namespace Telegram.Bot.Bookinator.CommandHandlers
{
    class EnglishHandler : ICommandHandler
    {
        string ICommandHandler.Name { get { return CommonConstants.CommandEnglish; } }
        void ICommandHandler.Execute(Session session, string[] parameters)
        {
            session.ChangeLanguage(CommonConstants.LanguageSymbolEnglish);
            Program.SendMessage(session, session.locale.GetMessageString(CommonConstants.MessageGreetings), "English command handler ,input = '" + String.Join(" ", parameters) + "'");
            session.Update();
        }
    }
}