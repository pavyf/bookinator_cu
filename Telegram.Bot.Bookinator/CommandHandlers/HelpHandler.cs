﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;

namespace Telegram.Bot.Bookinator.CommandHandlers
{
    class HelpHandler : ICommandHandler
    {
        string ICommandHandler.Name { get { return CommonConstants.CommandHelp; } }
        void ICommandHandler.Execute(Session session, string[] parameters)
        {
            Program.SendMessage(session, session.locale.GetMessageString(CommonConstants.MessageHelp), "Help command handler ,input = '" + String.Join(" ", parameters) + "'");
        }
    }
}