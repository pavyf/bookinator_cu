﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;

namespace Telegram.Bot.Bookinator.CommandHandlers
{
    class EditHandler : ICommandHandler
    {
        string ICommandHandler.Name { get { return CommonConstants.CommandEditBook; } }
        void ICommandHandler.Execute(Session session, string[] parameters)
        {
            Program.SendMessage(session, session.locale.GetMessageString(CommonConstants.MessageDefault), "Default response ,input = '" + String.Join(" ", parameters) + "'");
        }
    }
}
