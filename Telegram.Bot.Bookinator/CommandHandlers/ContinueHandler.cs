﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;

namespace Telegram.Bot.Bookinator.CommandHandlers
{
    class ContinueHandler: ICommandHandler
    {
        string ICommandHandler.Name { get { return CommonConstants.CommandContinue; } }// "continue"; } }
        void ICommandHandler.Execute(Session session, string[] parameters)
        {
            string Reply = session.GetNextLine();
            InlineKeyboardMarkup inlineKeyboard = new InlineKeyboardMarkup(Helper.GetInlineKeyboard(session.Position, session.Book.Lines.Count, session.IsReadingNote, session.locale));
            if (Reply == CommonConstants.EndOfFileFlag) Reply = session.locale.GetMessageString(CommonConstants.MessageEndOfFile);
            Program.SendMessage(session, Reply, "Continue handler, input = '" + String.Join(" ", parameters) + "'", inlineKeyboard);
        }
    }
}
