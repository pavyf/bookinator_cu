﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Telegram.Bot.Bookinator
{
    class Metric
    {
        private List<StatisticDataItem> _occurrences = new List<StatisticDataItem>();
        public string Name { get; }
        public List<StatisticDataItem> Datapoints { get { return _occurrences; } }
        private Type _type;
        private static Type[] AllowedTypes = new[] { typeof(int), typeof(float), typeof(long), typeof(double) };

        public Metric(string name)
        {
            Name = name;
            _type = typeof(int);
        }

        public Metric(string name, Type type)
        {
            if (AllowedTypes.FirstOrDefault(x => x == type) == null) throw new ArgumentException("Metric type not supported", "type");
            Name = name;
            _type = type;
        }

        public Metric(string name, Type type, List<StatisticDataItem> datapoints)
        {
            if (AllowedTypes.FirstOrDefault(x => x == type) == null) throw new ArgumentException("Metric type not supported", "type");
            Name = name;
            _type = type;
            _occurrences = datapoints;
        }

        public void Track(DateTime time, dynamic data = null)
        {
            if (data == null) data = 1;

            if (_type == data.GetType())
                _occurrences.Add(new StatisticDataItem(time, data));
            else
                throw new ArgumentException("Metric was initialized with datapoints of different type", "data");
        }

        public void TrackIncremental(DateTime time, dynamic data = null)
        {
            if (data == null) data = (_occurrences.Count > 0 ? _occurrences.Last().Data : 0) + 1;

            if (_type == data.GetType())
                _occurrences.Add(new StatisticDataItem(time, data));
            else
                throw new ArgumentException("Metric was initialized with datapoints of different type", "data");
        }

        public dynamic GetTotalDynamic(DateTime startTime, DateTime endTime, bool forceClear = false)
        {
            dynamic sum = 0;
            List<StatisticDataItem> stats = _occurrences.FindAll(x => (x.Time >= startTime) && (x.Time <= endTime));
            foreach (StatisticDataItem item in stats) sum += item.Data;

            if (forceClear) _occurrences.RemoveAll(x => x.Time < startTime);

            return sum;
        }

        public dynamic GetTotalDynamic()
        {
            dynamic sum = 0;
            foreach (StatisticDataItem item in _occurrences) sum += item.Data;
            return sum;
        }

        public string GetTotal(DateTime startTime, DateTime endTime, bool forceClear = false)
        {
            return GetTotalDynamic(startTime, endTime, forceClear).ToString();
        }

        public string GetTotal()
        {
            return GetTotalDynamic().ToString();
        }
        
        public dynamic GetAverageDynamic(DateTime startTime, DateTime endTime, bool forceClear = false)
        {
            dynamic sum = 0;
            List<StatisticDataItem> stats = _occurrences.FindAll(x => (x.Time >= startTime) && (x.Time <= endTime));
            foreach (StatisticDataItem item in stats) sum += item.Data;

            if (forceClear) _occurrences.RemoveAll(x => x.Time < startTime);

            return sum / (stats.Count != 0 ? stats.Count : 1);
        }

        public dynamic GetAverageDynamic()
        {
            dynamic sum = 0;
            foreach (StatisticDataItem item in _occurrences) sum += item.Data;
            return sum / (_occurrences.Count != 0 ? _occurrences.Count : 1);
        }

        public string GetAverage(DateTime startTime, DateTime endTime, bool forceClear = false)
        {
            return GetTotalDynamic(startTime, endTime, forceClear).ToString();
        }

        public string GetAverage()
        {
            return GetAverageDynamic().ToString();
        }

        public dynamic GetAverageTotalPerIntervalDynamic(DateTime startTime, DateTime endTime, TimeSpan interval, bool forceClear = false)
        {
            List<StatisticDataItem> stats = _occurrences.FindAll(x => (x.Time >= startTime) && (x.Time <= endTime));
            Metric blocks = new Metric("blocks", _type);
            foreach (StatisticDataItem item in stats)
                blocks.Track(item.Time, GetTotalDynamic(item.Time - interval, item.Time));

            if (forceClear) _occurrences.RemoveAll(x => x.Time < startTime);

            return blocks.GetAverageDynamic();
        }

        public string GetAverageTotalPerInterval(DateTime startTime, DateTime endTime, TimeSpan interval, bool forceClear = false)
        {
            return GetAverageTotalPerIntervalDynamic(startTime, endTime, interval, forceClear).ToString();
        }

        public dynamic GetAverageAveragePerIntervalDynamic(DateTime startTime, DateTime endTime, TimeSpan interval, bool forceClear = false)
        {
            List<StatisticDataItem> stats = _occurrences.FindAll(x => (x.Time >= startTime) && (x.Time <= endTime));
            Metric blocks = new Metric("blocks", _type);
            foreach (StatisticDataItem item in stats)
                blocks.Track(item.Time, GetAverageDynamic(item.Time - interval, item.Time));

            if (forceClear) _occurrences.RemoveAll(x => x.Time < startTime);

            return blocks.GetAverageDynamic();
        }

        public string GetAverageAveragePerInterval(DateTime startTime, DateTime endTime, TimeSpan interval, bool forceClear = false)
        {
            return GetAverageAveragePerIntervalDynamic(startTime, endTime, interval, forceClear).ToString();
        }

        public void Clear(DateTime startTime)
        {
            _occurrences.RemoveAll(x => x.Time < startTime);
        }

        private string FormatTimeSpanToString(TimeSpan span)
        {
            string formatted = string.Format("{0}{1}{2}{3}",
            span.Duration().Days > 0 ? string.Format("{0:0} day{1}, ", span.Days, span.Days == 1 ? String.Empty : "s") : string.Empty,
            span.Duration().Hours > 0 ? string.Format("{0:0} hour{1}, ", span.Hours, span.Hours == 1 ? String.Empty : "s") : string.Empty,
            span.Duration().Minutes > 0 ? string.Format("{0:0} minute{1}, ", span.Minutes, span.Minutes == 1 ? String.Empty : "s") : string.Empty,
            span.Duration().Seconds > 0 ? string.Format("{0:0} second{1}", span.Seconds, span.Seconds == 1 ? String.Empty : "s") : string.Empty);

            if (formatted.EndsWith(", ")) formatted = formatted.Substring(0, formatted.Length - 2);

            if (string.IsNullOrEmpty(formatted)) formatted = "0 seconds";

            return formatted;
        }

        private Image GetGraphByDataPoints(DateTime startTime, DateTime endTime, List<StatisticDataItem> datapoints, string GraphTitle)
        {
            Image graph = new Bitmap(500, 500);
            using (Graphics g = Graphics.FromImage(graph))
            {
                g.FillRectangle(new SolidBrush(Color.White), 0, 0, graph.Width, graph.Height);
                Pen axis = new Pen(Color.Black, 2);
                g.DrawLine(axis, 50, graph.Height - 50, graph.Width - 50, graph.Height - 50);
                g.DrawLine(axis, 50, 50, 50, graph.Height - 50);
                Pen line = new Pen(Color.Red, 1.5f);
                if (datapoints.Count > 1)
                {
                    double timeMultiplier = (graph.Width - 100) / ((endTime - startTime).TotalMilliseconds);
                    dynamic MinData = datapoints.Min(x => x.Data);
                    dynamic MaxData = datapoints.Max(x => x.Data);
                    float dataMultiplier = (float)(graph.Height - 100) / (MaxData - MinData + 1);
                    List<PointF> points = new List<PointF>();
                    datapoints.ForEach(x => points.Add(new PointF((float)((x.Time - startTime).TotalMilliseconds * timeMultiplier) + 50, (float)(graph.Height - 50) - (x.Data - MinData) * dataMultiplier)));
                    g.DrawLines(line, points.ToArray());
                    g.DrawString(startTime.ToShortTimeString(), SystemFonts.DefaultFont, Brushes.Black, 50, graph.Height - 35);
                    g.DrawString(endTime.ToShortTimeString(), SystemFonts.DefaultFont, Brushes.Black, graph.Width - 50, graph.Height - 35);
                    g.DrawString(MinData.ToString(), SystemFonts.DefaultFont, Brushes.Black, 10, graph.Height - 55);
                    g.DrawString(MaxData.ToString(), SystemFonts.DefaultFont, Brushes.Black, 10, 55);
                    g.DrawString(GraphTitle, SystemFonts.DefaultFont, Brushes.Black, 10, 20);
                }
            }
            return graph;
        }

        public Image GetGraph(DateTime startTime, DateTime endTime, bool forceClear = false)
        {
            List<StatisticDataItem> entries = _occurrences.FindAll(x => (x.Time >= startTime) && (x.Time <= endTime));

            if (forceClear) _occurrences.RemoveAll(x => x.Time < startTime);

            return GetGraphByDataPoints(startTime, endTime, entries, String.Format("Dynamics of {0} during past {1}", Name, FormatTimeSpanToString(endTime - startTime)));
        }

        public Image GetTotalGraph(DateTime startTime, DateTime endTime, bool forceClear = false)
        {
            List<StatisticDataItem> oldEntries = _occurrences.FindAll(x => (x.Time >= startTime) && (x.Time <= endTime));
            List<StatisticDataItem> entries = new List<StatisticDataItem>();
            foreach (StatisticDataItem entry in oldEntries)
                entries.Add(new StatisticDataItem(entry.Time, oldEntries.FindAll(x => x.Time <= entry.Time).Sum(x => x.Data)));


            if (forceClear) _occurrences.RemoveAll(x => x.Time < startTime);

            return GetGraphByDataPoints(startTime, endTime, entries, String.Format("Total number of {0} during past {1}", Name, FormatTimeSpanToString(endTime - startTime)));
        }

        public Image GetAverageTotalByIntervalGraph(DateTime startTime, DateTime endTime, TimeSpan interval, bool forceClear = false)
        {
            List<StatisticDataItem> oldEntries = _occurrences.FindAll(x => (x.Time >= startTime) && (x.Time <= endTime));
            List<StatisticDataItem> entries = new List<StatisticDataItem>();
            foreach (StatisticDataItem entry in oldEntries)
                entries.Add(new StatisticDataItem(entry.Time, GetAverageTotalPerIntervalDynamic(startTime, entry.Time, interval)));

            if (forceClear) _occurrences.RemoveAll(x => x.Time < startTime);

            return GetGraphByDataPoints(startTime, endTime, entries, String.Format("Average number of {0} in {2} during past {1}", Name, FormatTimeSpanToString(endTime - startTime), FormatTimeSpanToString(interval)));
        }

        public Image GetAverageAverageByIntervalGraph(DateTime startTime, DateTime endTime, TimeSpan interval, bool forceClear = false)
        {
            List<StatisticDataItem> oldEntries = _occurrences.FindAll(x => (x.Time >= startTime) && (x.Time <= endTime));
            List<StatisticDataItem> entries = new List<StatisticDataItem>();
            foreach (StatisticDataItem entry in oldEntries)
                entries.Add(new StatisticDataItem(entry.Time, GetAverageAveragePerIntervalDynamic(startTime, entry.Time, interval)));

            if (forceClear) _occurrences.RemoveAll(x => x.Time < startTime);

            return GetGraphByDataPoints(startTime, endTime, entries, String.Format("Average value of {0} in {2} during past {1}", Name, FormatTimeSpanToString(endTime - startTime), FormatTimeSpanToString(interval)));
        }
    }
}
 