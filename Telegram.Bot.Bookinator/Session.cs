﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.SQLite;
using System.Data.Common;
using System.Threading;
using Telegram.Bot.Types.ReplyMarkups;

namespace Telegram.Bot.Bookinator
{
    class Session
    {
        public enum ChatState
        {
            Commands = 0,
            Feedback = 1,
            BookEdit = 2,
            AccessLibrary = 3
        }

        public struct BookInfo
        {
            public string ID;
            public string Name;
            public int Position;
        }


        public const int LibrarySize = 10;


        private long _id;
        private int _position;
        private NoteItem _note = null;
        public List<BookInfo> LibraryInfo = new List<BookInfo>();
        public string localeSymbol = CommonConstants.LanguageSymbolEnglish;
        public int age = 0;
        public Book _book;
        public Localization locale { get { return CommonConstants.localizations.Find(x => x.symbol == localeSymbol); } }
        public ChatState state = ChatState.Commands;
        public int lastMessageID = -1;
        public bool oldVersion = true;

        private int AutoscrollDelay = 3000;
        private float AutoscrollMultiplier = 30f;
        private Task Autoscroll;
        private CancellationTokenSource StopAutoscrollToken;

        public string name { get; set; }

        public Book Book { get { return _book; } }

        public long ID { get { return _id; } }

        private async Task DoAutoscroll(CancellationToken CT)
        {
            await Task.Delay(3000);
            while (!CT.IsCancellationRequested)
            {
                string line = this.GetNextLine();
                Program.SendMessage(this, line, String.Format("Autoscroll for ChatID={0}", _id), Helper.GetAutoscrollKeyboard());
                if (line == CommonConstants.EndOfFileFlag)
                {
                    Program.SendMessage(this, "=====", "Break reply keyboard after autoscroll");
                    break;
                }
                await Task.Delay((int)(AutoscrollMultiplier * line.Length) + 1000);
            }
            Autoscroll = null;
        }

        public void AutoscrollFaster()
        {
            if (AutoscrollMultiplier > 10f) AutoscrollMultiplier /= 1.2f;
        }

        public void AutoscrollSlower()
        {
            if (AutoscrollMultiplier < 100f) AutoscrollMultiplier *= 1.2f;
        }

        public void ToggleAutoscroll()
        {
            if (Autoscroll == null)
            {
                StopAutoscrollToken = new CancellationTokenSource();
                Autoscroll = new Task(() => DoAutoscroll(StopAutoscrollToken.Token));
                Autoscroll.Start();
            }
            else
            {
                StopAutoscrollToken.Cancel();
            }
        }

        public void StartAutoscroll()
        {
            if (Autoscroll == null)
            {
                StopAutoscrollToken = new CancellationTokenSource();
                Autoscroll = new Task(() => DoAutoscroll(StopAutoscrollToken.Token));
                Autoscroll.Start();
            }
        }

        public void StopAutoscroll()
        {
            if (Autoscroll != null)
                StopAutoscrollToken.Cancel();
        }

        public int Position
        {
            get { return _position; }
            set { _position = value; }
        }
        public bool IsReadingNote { get { return (_note != null); } }

        /// <summary>
        /// Заводит сессию, обновляет базу если надо
        /// </summary>
        /// <param name="Id">Айдишник чата телеграма (не пользователя)</param>
        /// <param name="ChatName">Имя чата для хранения его в базе чтобы за чуваками следить</param>
        public Session(long Id, string ChatName, out bool isNew)
        {
            name = ChatName.Replace("'", "''");
            SQLiteCommand query = new SQLiteCommand(string.Format("SELECT CurrentBook, CurrentLine, Version, Locale FROM 'Sessions' WHERE TelegramID = {0};", Id), CommonConstants.databaseConnection);
            SQLiteDataReader queryResult = query.ExecuteReader();
            if (queryResult.Read())
            {
                _id = Id;
                try
                {
                    _book = new Book(string.Format(@"{0}\Books\{1}.book", CommonConstants.FullPath, queryResult.GetString(0)), CommonConstants.Filetype.BOOK);
                }
                catch (Exception e)
                {
                    _book = locale.defaultBook;
                    SQLiteCommand query2 = new SQLiteCommand(string.Format("UPDATE 'Sessions' SET CurrentBook = '{0}', CurrentLine = {1}, Name = '{3}', Version = {4} WHERE TelegramID = {2};", _book.id, 0, Id, name, CommonConstants.version), CommonConstants.databaseConnection);
                    query2.ExecuteNonQuery();
                }
                _position = queryResult.GetInt32(1);
                try
                {
                    oldVersion = (queryResult.GetInt32(2) < CommonConstants.version);
                }
                catch { }
                try
                {
                    localeSymbol = queryResult.GetString(3);
                }
                catch { }

                SQLiteCommand libraryQuery = new SQLiteCommand(string.Format("SELECT Book, Position FROM 'Library' WHERE TelegramID = {0};", Id), CommonConstants.databaseConnection);
                queryResult = libraryQuery.ExecuteReader();
                while (queryResult.Read())
                {
                    BookInfo LibraryBook = new BookInfo();
                    try
                    {
                        LibraryBook.ID = queryResult.GetString(0);
                        LibraryBook.Position = queryResult.GetInt32(1);
                        LibraryBook.Name = Book.GetBookName(LibraryBook.ID);
                        LibraryInfo.Add(LibraryBook);
                    }
                    catch (Exception e) { }
                }

                SQLiteCommand query3 = new SQLiteCommand(string.Format("UPDATE 'Sessions' SET Version = {0} WHERE TelegramID = {1};", CommonConstants.version, Id), CommonConstants.databaseConnection);
                query3.ExecuteNonQuery();
                isNew = false;
            }
            else
            {
                _id = Id;
                _position = 0;
                _book = locale.defaultBook;
                oldVersion = false;
                SQLiteCommand query2 = new SQLiteCommand(string.Format("INSERT INTO 'Sessions' ('TelegramID', 'CurrentBook', 'CurrentLine', 'Name', 'Version', 'Locale') VALUES ({0}, '{1}', {2}, '{3}', {4}, '{5}')", Id, _book.id, _position, name, CommonConstants.version, localeSymbol), CommonConstants.databaseConnection);
                query2.ExecuteNonQuery();
                isNew = true;
            }
            
        }

        public string GetNextLine(string noteID = "", bool ResetToBook = false)
        {
            string line = "";
            if (!String.IsNullOrEmpty(noteID))
            {
                _note = _book.Notes.Find(x => x.ID == noteID);
                if (_note != null) _note.Reset();
            }
            if (ResetToBook)
            {
                _note = null;
            }
            if (_note == null)
            {
                line = (_position < _book.Lines.Count) ? _book.Lines[_position++] : CommonConstants.EndOfFileFlag;
            }
            else
            {
                if (_note.Position == _note.Content.Count)
                {
                    line = (_position < _book.Lines.Count) ? _book.Lines[_position++] : CommonConstants.EndOfFileFlag;
                    _note.Reset();
                    _note = null;
                }
                else
                {
                    line = _note.Content[_note.Position++];
                }
            }
            Update();
            return line;
        }

        public void ChangeLanguage(string symbol = CommonConstants.LanguageSymbolRussian)
        {
            bool isNewish = (_position < 30) && (_book == locale.defaultBook);
            localeSymbol = symbol;
            if (isNewish)
            {
                _book = locale.defaultBook;
                _position = 0;
            }
        }

        public void NewBook(Stream filename, CommonConstants.Filetype type = CommonConstants.Filetype.TXT)
        {
            _position = 0;
            _book = new Book(filename, type);
            Update();
        }

        public void Update()
        {
            int index = LibraryInfo.FindIndex(x => x.ID == _book.id);
            if (index != -1)
            {
                BookInfo newBI = LibraryInfo[index];
                newBI.Position = _position;
                LibraryInfo[index] = newBI;
            }
            SQLiteCommand query = new SQLiteCommand(string.Format("UPDATE 'Sessions' SET CurrentBook = '{0}', CurrentLine = {1}, Name = '{3}', Version = {4}, Locale = '{5}' WHERE TelegramID = {2};", _book.id, _position, _id, name, CommonConstants.version, localeSymbol), CommonConstants.databaseConnection);
            query.ExecuteNonQuery();
        }

        public bool SaveCurrentBookToLibrary()
        {
            try
            {
                //if (LibraryInfo.Count >= LibrarySize) return false;
                SQLiteCommand updateCommand = new SQLiteCommand(string.Format("INSERT INTO 'Library' ('TelegramID', 'Book', 'Position') VALUES ({0}, '{1}', {2});", _id, _book.id, _position), CommonConstants.databaseConnection);
                updateCommand.ExecuteNonQuery();
                BookInfo newBook = new BookInfo();
                newBook.ID = _book.id;
                newBook.Name = _book.getName();
                newBook.Position = _position;
                LibraryInfo.Add(newBook);
            }
            catch (Exception e) { return false; }
            return true;
        }

        public bool SwitchToBookFromLibrary(string BookID)
        {
            if (LibraryInfo.FindIndex(x => x.ID == BookID) == -1)
                return false;
            else
            {
                int index = LibraryInfo.FindIndex(x => x.ID == _book.id);
                if (index != -1)
                {
                    BookInfo newBI = LibraryInfo[index];
                    newBI.Position = _position;
                    LibraryInfo[index] = newBI;
                }
                SQLiteCommand query = new SQLiteCommand(string.Format("SELECT * FROM 'Library' WHERE TelegramID = {0} AND Book = '{1}';", _id, _book.id), CommonConstants.databaseConnection);
                SQLiteDataReader reader = query.ExecuteReader();
                if (!reader.Read())
                {
                    SQLiteCommand updateCommand = new SQLiteCommand(string.Format("INSERT INTO 'Library' ('TelegramID', 'Book', 'Position') VALUES ({0}, '{1}', {2});", _id, _book.id, _position), CommonConstants.databaseConnection);
                    updateCommand.ExecuteNonQuery();
                }
                else
                {
                    SQLiteCommand updateCommand = new SQLiteCommand(string.Format("UPDATE 'Library' SET Position = {2} WHERE TelegramID = {0} AND Book = '{1}';", _id, _book.id, _position), CommonConstants.databaseConnection);
                    updateCommand.ExecuteNonQuery();
                }
                _book = new Book(string.Format(@"{0}\Books\{1}.book", CommonConstants.FullPath, BookID), CommonConstants.Filetype.BOOK);
                _position = LibraryInfo.Find(x => x.ID == BookID).Position;
            }
            return true;
        }

        public List<string> GetLibraryContent()
        {
            List<string> content = new List<string>();
            foreach (BookInfo book in LibraryInfo)
                content.Add(book.Name);
            return content;
        }

        public bool RemoveBookFromLibrary(string BookID)
        {
            if (LibraryInfo.FindIndex(x => x.ID == BookID) == -1)
                return false;
            else
            {
                SQLiteCommand updateCommand = new SQLiteCommand(string.Format("DELETE FROM 'Library' WHERE TelegramID = {0} AND Book = '{1}';", _id, BookID), CommonConstants.databaseConnection);
                updateCommand.ExecuteNonQuery();
                LibraryInfo.RemoveAll(x => x.ID == BookID);
            }              
            return true;
        }
        

    }
}
