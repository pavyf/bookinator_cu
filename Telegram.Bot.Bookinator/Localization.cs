﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Telegram.Bot.Bookinator
{
    class Localization
    {

        #region string arrays
        List<Constant> Messages = new List<Constant>();

        List<Constant> Descriptions = new List<Constant>();

        List<Constant> Pictures = new List<Constant>();

        Dictionary<string, string> Buttons = new Dictionary<string, string>();
        
        #endregion
        public string symbol = string.Empty;
        public Book defaultBook;

        public Localization(string symbol = CommonConstants.LanguageSymbolRussian)
        {
            this.symbol = symbol;
            defaultBook = new Book(string.Format(@"{0}\Books\Default_{1}.book", CommonConstants.FullPath, symbol), CommonConstants.Filetype.BOOK);
            XmlDocument locale = new XmlDocument();
            locale.Load(String.Format("{1}\\Localizations\\{0}.locale", symbol, CommonConstants.FullPath));
            XmlElement root = locale.DocumentElement;
            List<Constant> msgs = new List<Constant>();
            foreach (XmlNode node in root)
            {
                string type = node.Name;
                string name = node.Attributes.GetNamedItem("string").Value;
                string value = node.InnerText.Trim();
                switch (type)
                {
                    case "description":
                        {
                            Descriptions.Add(new Constant() { Name = name, Value = value });
                            break;
                        }
                    case "message":
                        {
                            msgs.Add(new Constant() { Name = name, Value = value });
                            break;
                        }
                    case "picture":
                        {
                            Pictures.Add(new Constant() { Name = name, Value = value });
                            break;
                        }
                    case "button":
                        {
                            Buttons.Add(name, value);
                            break;
                        }
                }
            }
            foreach (Constant message in msgs)
                Messages.Add(new Constant() { Name = message.Name, Value = TrimMessage(FormatMessageString(message.Value)) });
        }

        private string FormatMessageString(string str)
        {
            foreach (Constant desc in Descriptions)
            {
                str = str.Replace(String.Format("%{0}%", desc.Name), desc.Value);
            }

            str = str.Replace(String.Format("%{0}%", CommonConstants.CommandReferenceStart), CommonConstants.CommandStart);
            str = str.Replace(String.Format("%{0}%", CommonConstants.CommandReferenceContinue), CommonConstants.CommandContinue);
            str = str.Replace(String.Format("%{0}%", CommonConstants.CommandReferenceLibrary), CommonConstants.CommandLibrary);
            str = str.Replace(String.Format("%{0}%", CommonConstants.CommandReferenceReset), CommonConstants.CommandReset);
            str = str.Replace(String.Format("%{0}%", CommonConstants.CommandReferenceLanguage), CommonConstants.CommandLanguage);
            str = str.Replace(String.Format("%{0}%", CommonConstants.CommandReferenceHelp), CommonConstants.CommandHelp);
            str = str.Replace(String.Format("%{0}%", CommonConstants.CommandReferenceFeedback), CommonConstants.CommandFeedback);
            str = str.Replace(String.Format("%{0}%", CommonConstants.CommandReferenceAutoscroll), CommonConstants.CommandAutoscroll);

            str = str.Replace(String.Format("%{0}%", CommonConstants.CommandReferenceCancel), CommonConstants.CommandCancel);
            str = str.Replace(String.Format("%{0}%", CommonConstants.CommandReferenceRussian), CommonConstants.CommandRussian);
            str = str.Replace(String.Format("%{0}%", CommonConstants.CommandReferenceEnglish), CommonConstants.CommandEnglish);
            str = str.Replace(String.Format("%{0}%", CommonConstants.CommandReferenceEditBook), CommonConstants.CommandEditBook);

            return str;
        }

        private string TrimMessage(string str)
        {
            string[] lines = str.Split(new[] { "\r\n" }, StringSplitOptions.None);
            str = "";
            foreach (string st in lines)
            {
                str += st.Trim() + "\r\n";
            }
            return str.Trim();
        }

        public string GetMessageString(string name)
        {
            Constant message = Messages.Find(x => x.Name == name);
            if (message.Value == null) message = Pictures.Find(x => x.Name == name);
            return (message.Value == null) ? String.Format("%{0}%", name) : message.Value;
        }

        public string GetButtonString(string name)
        {
            try
            {
                return Buttons[name];
            }
            catch
            {
                return String.Format("%{0}%", name);
            }
        }

    }
}
