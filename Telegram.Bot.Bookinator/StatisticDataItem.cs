﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Telegram.Bot.Bookinator
{
    class StatisticDataItem
    {
        private DateTime _time;
        private dynamic _data;

        public DateTime Time { get { return _time; } }
        public dynamic Data { get { return _data; } set { _data = value; } }

        public StatisticDataItem()
        {
            _time = DateTime.Now;
            _data = null;
        }

        public StatisticDataItem(DateTime time, dynamic data = null)
        {
            _time = time;
            _data = data;
        }
    }
}
