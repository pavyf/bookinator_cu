﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Telegram.Bot.Bookinator
{
    class NoteItem
    {
        private int _position;
        private string _id;
        private List<string> _content = new List<string>();

        public int Position { get { return _position; } set { _position = value; } }
        public string ID { get { return _id; } }
        public List<string> Content { get { return _content; } set { _content = value; } }

        public NoteItem(string Id, int Position = 0)
        {
            _id = Id;
            _position = Position;
        }

        public void Reset() { _position = 0; }
    }
}
