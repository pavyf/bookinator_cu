﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Telegram.Bot.Bookinator
{
    class CommandList
    {
        public List<ICommandHandler> Commands { get { return _commands; } }
        public List<ICommandHandler> ChatStateHandlers(Session.ChatState state)
        {
            return _chatStateMap[state];
        }

        private List<ICommandHandler> _commands = new List<ICommandHandler>();
        private Dictionary<Session.ChatState, List<ICommandHandler>> _chatStateMap = new Dictionary<Session.ChatState, List<ICommandHandler>>();

        public CommandList()
        {
            foreach (Session.ChatState state in Enum.GetValues(typeof(Session.ChatState)))
                _chatStateMap.Add(state, new List<ICommandHandler>());
        }

        public void AddHandler(ICommandHandler handler)
        {
            _commands.Add(handler);
        }

        public void AddHandler(ICommandHandler handler, params Session.ChatState[] chatStates)
        {
            _commands.Add(handler);
            foreach (Session.ChatState state in chatStates)
                _chatStateMap[state].Add(handler);
        }

        public void AddHandlers(List<ICommandHandler> handlers)
        {
            _commands.AddRange(handlers);
        }

        public void AddHandlers(List<ICommandHandler> handlers, params Session.ChatState[] chatStates)
        {
            _commands.AddRange(handlers);
            foreach (Session.ChatState state in chatStates)
                _chatStateMap[state].AddRange(handlers);
        }

        public void AddHandlerState(ICommandHandler handler, Session.ChatState chatState)
        {
            if (!_commands.Contains(handler))
                _commands.Add(handler);

            if (!_chatStateMap[chatState].Contains(handler))
                _chatStateMap[chatState].Add(handler);
        }

        public void RemoveHandlerState(ICommandHandler handler, Session.ChatState chatState)
        {
            if (!_commands.Contains(handler))
                _commands.Add(handler);

            if (_chatStateMap[chatState].Contains(handler))
                _chatStateMap[chatState].Remove(handler);
        }


    }
}
