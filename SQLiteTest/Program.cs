﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Common;
using System.Data.SQLite;
using System.IO;

namespace SQLiteTest
{
    class Program
    {
        static void Main(string[] args)
        {
            string dbName = @"Sessions\sessions.db";
            SQLiteConnection.CreateFile(dbName);
            Console.WriteLine(File.Exists(dbName) ? "База данных создана" : "Возникла ошиюка при создании базы данных");
            Console.ReadKey(true);
        }
    }
}
