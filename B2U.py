#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sqlite3
import telegram
import logging
import xml.etree.ElementTree as ET
import time

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
bot = telegram.Bot(token='')

f = open('stderr.txt', 'w')
try:
	bot.sendMessage(parse_mode='HTML', chat_id=, text='Привет! Спасибо за отзыв! Обязательно сделаем автоскролл, и введем его, как только отладим и настроим его работу. Если будут еще вопросы или предложения, /feedback всегда здесь 😉\n\n Приятного чтения!')
except telegram.TelegramError as e:
	f.write(str(e) + '\n')
f.close()