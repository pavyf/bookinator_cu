#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sqlite3
import telegram
import logging
import xml.etree.ElementTree as ET
import time

def getMessage(path, msg):
	tree = ET.parse(path)
	root = tree.getroot()
	for attr in root.iter('message'):
		string = attr.get('string')
		if string == msg:
			return attr.text.replace("\n    ", "\n")


pathtodb = 'Telegram.Bot.Bookinator\\bin\\Debug\\Sessions\\sessions.db'
ruLocalePath = 'Telegram.Bot.Bookinator\Localizations\\RU-ru.locale'
enLocalePath = 'Telegram.Bot.Bookinator\\Localizations\\EN-en.locale'

ruMessage = getMessage(ruLocalePath, "msg_new_version")
enMessage = getMessage(enLocalePath, "msg_new_version")

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
bot = telegram.Bot(token='')

conn = sqlite3.connect(pathtodb)
conn.row_factory = lambda cursor, row: row[0]
c = conn.cursor()
ids = c.execute('SELECT TelegramID FROM Sessions').fetchall()
locales = c.execute('SELECT Locale FROM Sessions').fetchall()
f = open('stderr.txt', 'w')
for idx, id in enumerate(ids):
	time.sleep(1)
	if locales[idx] == "ru-RU":
		try:
			bot.sendMessage(parse_mode='HTML', chat_id=id, text=ruMessage)
		except telegram.TelegramError as e:
			f.write(str(e) + '\n')
	elif locales[idx] == "en-EN":
		try:
			bot.sendMessage(parse_mode='HTML', chat_id=id, text=enMessage)
		except telegram.TelegramError as e:
			f.write(str(e) + '\n')

f.close()